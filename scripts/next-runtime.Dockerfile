FROM node:19-alpine

RUN sed -i "s/dl-cdn.alpinelinux.org/mirrors.aliyun.com/g" /etc/apk/repositories

RUN apk add --no-cache --virtual .build-deps \
	    curl bash vim htop

RUN yarn config set registry https://registry.npm.taobao.org/

WORKDIR /var/services
