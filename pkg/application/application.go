package application

import (
	"gitee.com/eden-framework/context"
	"gitee.com/eden-framework/eden-framework/internal"
	"gitee.com/eden-framework/eden-framework/internal/generator"
	"gitee.com/eden-framework/eden-framework/internal/project"
	"gitee.com/eden-framework/eden-framework/pkg/autoconf"
	"gitee.com/eden-framework/eden-framework/pkg/constant/enum"
	str "gitee.com/eden-framework/strings"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"testing"
)

type Application struct {
	ctx                *context.WaitStopContext
	p                  *project.Project
	cmd                *cobra.Command
	configManager      *autoconf.ConfigManager
	envConfigPrefix    string
	configCenterType   enum.ConfigCenterType
	configCenterOption any
	envConfig          []any

	onInit       []func() error
	onInitStrict bool
}

func NewApplication(
	runner func(ctx *context.WaitStopContext, t *testing.T) error,
	t *testing.T,
	opts ...Option,
) *Application {
	p := &project.Project{}
	ctx := context.NewWaitStopContext()
	err := p.UnmarshalFromFile("", "")
	if err != nil {
		logrus.Panic(err)
	}

	app := &Application{
		p:   p,
		ctx: ctx,
	}

	for _, opt := range opts {
		opt(app)
	}

	app.cmd = &cobra.Command{
		Use:   app.p.Name,
		Short: app.p.Desc,
		Run: func(cmd *cobra.Command, args []string) {
			if len(app.onInit) > 0 {
				for _, initializer := range app.onInit {
					err := initializer()
					if err != nil && app.onInitStrict {
						panic(err)
					}
				}
			}

			if t != nil {
				err := runner(ctx, t)
				if err != nil {
					panic(err)
				}
			} else {
				go runner(ctx, nil)
				app.WaitStop(
					func(ctx *context.WaitStopContext) error {
						ctx.Cancel()
						return nil
					},
				)
			}
		},
	}
	app.cmd.PersistentFlags().StringVarP(&app.envConfigPrefix, "env-prefix", "e", app.p.Name, "prefix for env var")
	app.envConfigPrefix = str.ToUpperSnakeCase(app.envConfigPrefix)

	app.configManager = autoconf.NewConfigManager(
		autoconf.Options{
			ConfigPath:          "",
			EnvConfigPrefix:     app.envConfigPrefix,
			ConfigCenterType:    app.configCenterType,   // TODO
			ConfigCenterOptions: app.configCenterOption, // TODO
		}, app.envConfig...,
	)

	return app
}

func (app *Application) AddCommand(cmd ...*cobra.Command) {
	app.cmd.AddCommand(cmd...)
}

func (app *Application) Start() {
	os.Setenv(internal.EnvVarKeyProjectName, app.p.Name)
	os.Setenv(internal.EnvVarKeyServiceName, strings.Replace(app.p.Name, "service-", "", 1))
	os.Setenv(internal.EnvVarKeyProjectGroup, app.p.Group)

	// 生成配置中心默认配置文件
	if app.configCenterType != enum.CONFIG_CENTER_TYPE_UNKNOWN {
		configCenterEnvVars := autoconf.GetDefaultFromEnv("", []any{app.configCenterOption})
		if os.Getenv("GOENV") != "PROD" {
			cwd, _ := os.Getwd()
			configGenerator := generator.NewConfigGenerator(configCenterEnvVars, "./configs/config_center.default.yml")
			generator.Generate(configGenerator, cwd, cwd)
		}
	}

	defaultEnvVars := autoconf.GetDefaultFromEnv(app.envConfigPrefix, app.envConfig)
	if os.Getenv("GOENV") != "PROD" {
		cwd, _ := os.Getwd()

		configGenerator := generator.NewConfigGenerator(defaultEnvVars, "./configs/default.yml")
		generator.Generate(configGenerator, cwd, cwd)

		generate := generator.NewDockerGenerator(app.p.Name, defaultEnvVars)
		generator.Generate(generate, cwd, cwd)

		k8sGenerator := generator.NewK8sGenerator(app.envConfig, defaultEnvVars)
		generator.Generate(k8sGenerator, cwd, cwd)
	}

	app.configManager.MustAutoconf()

	if err := app.cmd.Execute(); err != nil {
		logrus.Error(err)
		os.Exit(1)
	}
}

func (app *Application) WaitStop(clearFunc func(ctx *context.WaitStopContext) error) {
	sig := make(chan os.Signal, 1)
	signal.Notify(
		sig,
		syscall.SIGHUP,
		syscall.SIGINT,
		syscall.SIGTERM,
		syscall.SIGQUIT,
	)

	s := <-sig
	err := clearFunc(app.ctx)
	if err != nil {
		logrus.Errorf("shutdown with error: %v", err)
	} else {
		logrus.Infof("graceful shutdown with signal: %s", s.String())
	}
}

func (app *Application) Context() *context.WaitStopContext {
	return app.ctx
}
