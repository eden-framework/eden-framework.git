package enum

import "github.com/AlecAivazis/survey/v2/core"

// api:enum
//
//go:generate eden generate enum --type-name=ConfigCenterType
type ConfigCenterType uint8

const (
	CONFIG_CENTER_TYPE_UNKNOWN ConfigCenterType = iota
	CONFIG_CENTER_TYPE__NACOS                   // Nacos
	CONFIG_CENTER_TYPE__APOLLO                  // Apollo
	CONFIG_CENTER_TYPE__ETCD                    // Etcd
)

func (v ConfigCenterType) StringEnums() (result []string) {
	enums := v.Enums()
	result = append(result, "NONE")
	for _, enum := range enums {
		result = append(result, enum[0])
	}
	return
}

func (v *ConfigCenterType) WriteAnswer(field string, value interface{}) error {
	result := value.(core.OptionAnswer)
	*v, _ = ParseConfigCenterTypeFromString(result.Value)
	return nil
}
