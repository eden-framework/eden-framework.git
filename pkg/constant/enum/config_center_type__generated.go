package enum

import (
	"bytes"
	"encoding"
	"errors"

	gitee_com_eden_framework_enumeration "gitee.com/eden-framework/enumeration"
)

var InvalidConfigCenterType = errors.New("invalid ConfigCenterType")

func init() {
	gitee_com_eden_framework_enumeration.RegisterEnums("ConfigCenterType", map[string]string{
		"ETCD":   "Etcd",
		"APOLLO": "Apollo",
		"NACOS":  "Nacos",
	})
}

func ParseConfigCenterTypeFromString(s string) (ConfigCenterType, error) {
	switch s {
	case "":
		return CONFIG_CENTER_TYPE_UNKNOWN, nil
	case "ETCD":
		return CONFIG_CENTER_TYPE__ETCD, nil
	case "APOLLO":
		return CONFIG_CENTER_TYPE__APOLLO, nil
	case "NACOS":
		return CONFIG_CENTER_TYPE__NACOS, nil
	}
	return CONFIG_CENTER_TYPE_UNKNOWN, InvalidConfigCenterType
}

func ParseConfigCenterTypeFromLabelString(s string) (ConfigCenterType, error) {
	switch s {
	case "":
		return CONFIG_CENTER_TYPE_UNKNOWN, nil
	case "Etcd":
		return CONFIG_CENTER_TYPE__ETCD, nil
	case "Apollo":
		return CONFIG_CENTER_TYPE__APOLLO, nil
	case "Nacos":
		return CONFIG_CENTER_TYPE__NACOS, nil
	}
	return CONFIG_CENTER_TYPE_UNKNOWN, InvalidConfigCenterType
}

func (ConfigCenterType) EnumType() string {
	return "ConfigCenterType"
}

func (ConfigCenterType) Enums() map[int][]string {
	return map[int][]string{
		int(CONFIG_CENTER_TYPE__ETCD):   {"ETCD", "Etcd"},
		int(CONFIG_CENTER_TYPE__APOLLO): {"APOLLO", "Apollo"},
		int(CONFIG_CENTER_TYPE__NACOS):  {"NACOS", "Nacos"},
	}
}

func (v ConfigCenterType) String() string {
	switch v {
	case CONFIG_CENTER_TYPE_UNKNOWN:
		return ""
	case CONFIG_CENTER_TYPE__ETCD:
		return "ETCD"
	case CONFIG_CENTER_TYPE__APOLLO:
		return "APOLLO"
	case CONFIG_CENTER_TYPE__NACOS:
		return "NACOS"
	}
	return "UNKNOWN"
}

func (v ConfigCenterType) Label() string {
	switch v {
	case CONFIG_CENTER_TYPE_UNKNOWN:
		return ""
	case CONFIG_CENTER_TYPE__ETCD:
		return "Etcd"
	case CONFIG_CENTER_TYPE__APOLLO:
		return "Apollo"
	case CONFIG_CENTER_TYPE__NACOS:
		return "Nacos"
	}
	return "UNKNOWN"
}

var _ interface {
	encoding.TextMarshaler
	encoding.TextUnmarshaler
} = (*ConfigCenterType)(nil)

func (v ConfigCenterType) MarshalText() ([]byte, error) {
	str := v.String()
	if str == "UNKNOWN" {
		return nil, InvalidConfigCenterType
	}
	return []byte(str), nil
}

func (v *ConfigCenterType) UnmarshalText(data []byte) (err error) {
	*v, err = ParseConfigCenterTypeFromString(string(bytes.ToUpper(data)))
	return
}
