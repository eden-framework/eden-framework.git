package autoconf

import (
	"gitee.com/eden-framework/apollo"
	"os"
)

func FromApollo(apolloConfig *apollo.ApolloBaseConfig, conf []interface{}) error {
	if apolloConfig == nil || os.Getenv("GOENV") == "LOCAL" {
		return nil
	}

	return apollo.AssignConfWithDefault(*apolloConfig, conf...)
}
