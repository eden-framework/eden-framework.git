package autoconf

import (
	"gitee.com/eden-framework/nacos"
	"github.com/sirupsen/logrus"
)

func FromNacos(logLevel logrus.Level, nacosConfig *nacos.BaseConfig, prefix string, conf []any) error {
	if nacosConfig == nil || !nacosConfig.NacosConfigEnable {
		return nil
	}

	return nacos.AssignConfWithDefault(*nacosConfig, logLevel, prefix, conf...)
}
