package autoconf

import (
	"errors"
	"gitee.com/eden-framework/apollo"
	"gitee.com/eden-framework/eden-framework/pkg/constant/enum"
	"gitee.com/eden-framework/envconfig"
	"gitee.com/eden-framework/etcd"
	"gitee.com/eden-framework/nacos"
	"github.com/sirupsen/logrus"
	"os"
	"path"
)

const (
	DefaultConfigPath = "./configs"
)

var (
	ErrConfigCenterOptionsType = errors.New("config center options type error")
)

type Options struct {
	LogLevel            logrus.Level
	ConfigPath          string
	EnvConfigPrefix     string
	ConfigCenterType    enum.ConfigCenterType
	ConfigCenterOptions any
}

// ConfigManager 配置自动装配工具，依照优先读取本地配置文件、环境变量、配置中心的顺序进行配置的自动装配
type ConfigManager struct {
	logLevel logrus.Level
	// ConfigPath 配置文件路径
	configPath string
	// envConfigPrefix 环境变量前缀
	envConfigPrefix string
	// configCenterType 配置中心类型
	configCenterType enum.ConfigCenterType
	// configCenterOptions 配置中心选项
	configCenterOptions any
	// Configs 配置列表（必须是地址指针）
	configs []any
}

// NewConfigManager 创建配置自动装配工具
func NewConfigManager(opt Options, configs ...any) *ConfigManager {
	if opt.ConfigPath == "" {
		opt.ConfigPath = DefaultConfigPath
	}
	return &ConfigManager{
		logLevel:            opt.LogLevel,
		configPath:          opt.ConfigPath,
		envConfigPrefix:     opt.EnvConfigPrefix,
		configCenterType:    opt.ConfigCenterType,
		configCenterOptions: opt.ConfigCenterOptions,
		configs:             configs,
	}
}

func (m *ConfigManager) MustAutoconf() {
	err := m.loadLocalConfig()
	if err != nil {
		logrus.Panicf("load local config failed: %s", err.Error())
	}
	err = m.loadEnvConfig()
	if err != nil {
		logrus.Panicf("load env config failed: %s", err.Error())
	}
	err = m.loadConfigCenterConfig()
	if err != nil {
		logrus.Panicf("load config center config failed: %s", err.Error())
	}
	Initialize(m.configs...)
}

func (m *ConfigManager) Autoconf() error {
	err := m.loadLocalConfig()
	if err != nil {
		return err
	}
	err = m.loadEnvConfig()
	if err != nil {
		return err
	}
	return m.loadConfigCenterConfig()
}

func (m *ConfigManager) loadLocalConfig() error {
	// attempt to load config_center.default.yml
	_ = envconfig.LoadDefaultFromYml(path.Join(m.configPath, "config_center.default.yml"))
	if os.Getenv("GOENV") == "LOCAL" {
		// attempt to load config_center.local.yml
		_ = envconfig.LoadDefaultFromYml(path.Join(m.configPath, "config_center.local.yml"))
	}

	// attempt to load default.yml
	_ = envconfig.LoadDefaultFromYml(path.Join(m.configPath, "default.yml"))
	if os.Getenv("GOENV") == "LOCAL" {
		// attempt to load local.yml
		_ = envconfig.LoadDefaultFromYml(path.Join(m.configPath, "local.yml"))
	}

	return nil
}

func (m *ConfigManager) loadEnvConfig() error {
	if m.configCenterType != enum.CONFIG_CENTER_TYPE_UNKNOWN {
		err := FromEnv(m.logLevel, "", []any{m.configCenterOptions})
		if err != nil {
			return err
		}
	}
	return FromEnv(m.logLevel, m.envConfigPrefix, m.configs)
}

func (m *ConfigManager) loadConfigCenterConfig() (err error) {
	if m.configCenterType == enum.CONFIG_CENTER_TYPE__APOLLO {
		if c, ok := m.configCenterOptions.(*apollo.ApolloBaseConfig); ok {
			err = FromApollo(c, m.configs)
		} else {
			return ErrConfigCenterOptionsType
		}
	} else if m.configCenterType == enum.CONFIG_CENTER_TYPE__NACOS {
		if c, ok := m.configCenterOptions.(*nacos.BaseConfig); ok {
			err = FromNacos(m.logLevel, c, m.envConfigPrefix, m.configs)
		} else {
			return ErrConfigCenterOptionsType
		}
	} else if m.configCenterType == enum.CONFIG_CENTER_TYPE__ETCD {
		if c, ok := m.configCenterOptions.(*etcd.BaseConfig); ok {
			err = FromEtcd(c, m.envConfigPrefix, m.configs)
		} else {
			return ErrConfigCenterOptionsType
		}
	}
	return err
}
