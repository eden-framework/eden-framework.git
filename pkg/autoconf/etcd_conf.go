package autoconf

import (
	"gitee.com/eden-framework/etcd"
)

func FromEtcd(config *etcd.BaseConfig, prefix string, conf []any) error {
	if config == nil || !config.EtcdConfigEnable {
		return nil
	}

	return etcd.AssignConfWithDefault(config, prefix, conf...)
}
