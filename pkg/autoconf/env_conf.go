package autoconf

import (
	"gitee.com/eden-framework/envconfig"
	"github.com/sirupsen/logrus"
)

func FromEnv(logLevel logrus.Level, confPrefix string, conf []interface{}) error {
	for _, c := range conf {
		err := envconfig.Process(confPrefix, c)
		if err != nil {
			return err
		}

		if logLevel == logrus.DebugLevel {
			_ = envconfig.Usage(confPrefix, c, true)
		}
	}

	return nil
}

func GetDefaultFromEnv(confPrefix string, conf []interface{}) []envconfig.EnvVar {
	var defaultEnvVars = make([]envconfig.EnvVar, 0)
	for _, c := range conf {
		envs, err := envconfig.GatherInfo(confPrefix, c)
		if err != nil {
			logrus.Panic(err)
		}
		defaultEnvVars = append(defaultEnvVars, envs...)
	}
	return defaultEnvVars
}
