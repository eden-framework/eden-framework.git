package autoconf

import (
	"gitee.com/eden-framework/reflectx"
	"reflect"
)

type Initializer interface {
	Init()
}

func Initialize(config ...any) {
	for _, c := range config {
		rv := reflectx.Indirect(reflect.ValueOf(c))
		for i := 0; i < rv.NumField(); i++ {
			value := rv.Field(i)
			if conf, ok := value.Interface().(Initializer); ok {
				conf.Init()
			}
		}
	}
}
