package workflows

import (
	"gitee.com/eden-framework/eden-framework/internal/project"
)

func init() {
	project.RegisterWorkFlow("main-only", MasterOnly)
}

var MasterOnly = &project.Workflow{
	BranchFlows: project.BranchFlows{
		"main": {
			Env: map[string]string{
				"GOENV": "STAGING",
			},
			Jobs: project.Jobs{
				DefaultJobForTest,
				DefaultJobForBuild,
				DefaultJobForShip,
				DefaultJobForDeploy,
			},
		},
	},
}
