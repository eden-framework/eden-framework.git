package scanner

import (
	"fmt"
	"gitee.com/eden-framework/packagex"
	"gitee.com/eden-framework/sqlx/builder"
	"go/types"
	"k8s.io/utils/pointer"
	"strings"
)

type ModelScannerConfig struct {
	StructName string
	TableName  string

	WithIDClient       *bool
	WithReadPagination *bool

	FieldKeyUniqueID  string
	FieldKeyDeletedAt string
	FieldKeyCreatedAt string
	FieldKeyUpdatedAt string

	ControllerPath string
	ErrorPath      string
}

func (g *ModelScannerConfig) SetDefaults() {
	if g.FieldKeyDeletedAt == "" {
		g.FieldKeyDeletedAt = "DeletedAt"
	}

	if g.FieldKeyCreatedAt == "" {
		g.FieldKeyCreatedAt = "CreatedAt"
	}

	if g.FieldKeyUpdatedAt == "" {
		g.FieldKeyUpdatedAt = "UpdatedAt"
	}

	if g.TableName == "" {
		g.TableName = toDefaultTableName(g.StructName)
	}

	if g.WithIDClient == nil {
		g.WithIDClient = pointer.BoolPtr(true)
	}
	if g.WithReadPagination == nil {
		g.WithReadPagination = pointer.BoolPtr(true)
	}
}

type Keys struct {
	Primary       []string
	Indexes       builder.Indexes
	UniqueIndexes builder.Indexes
}

func (ks *Keys) PatchUniqueIndexesWithSoftDelete(softDeleteField string) {
	if len(ks.UniqueIndexes) > 0 {
		for name, fieldNames := range ks.UniqueIndexes {
			ks.UniqueIndexes[name] = stringUniq(append(fieldNames, softDeleteField))
		}
	}
}

func (ks *Keys) Bind(table *builder.Table) {
	if len(ks.Primary) > 0 {
		cols, err := table.Fields(ks.Primary...)
		if err != nil {
			panic(fmt.Errorf("%s, please check primary def", err))
		}
		ks.Primary = cols.FieldNames()
		table.AddKey(builder.PrimaryKey(cols))
	}

	if len(ks.UniqueIndexes) > 0 {
		for indexNameAndMethod, fieldNames := range ks.UniqueIndexes {
			indexName, method := builder.ResolveIndexNameAndMethod(indexNameAndMethod)
			cols, err := table.Fields(fieldNames...)
			if err != nil {
				panic(fmt.Errorf("%s, please check unique_index def", err))
			}
			ks.UniqueIndexes[indexNameAndMethod] = cols.FieldNames()
			table.AddKey(builder.UniqueIndex(indexName, cols).Using(method))
		}
	}

	if len(ks.Indexes) > 0 {
		for indexNameAndMethod, fieldNames := range ks.Indexes {
			indexName, method := builder.ResolveIndexNameAndMethod(indexNameAndMethod)
			cols, err := table.Fields(fieldNames...)
			if err != nil {
				panic(fmt.Errorf("%s, please check index def", err))
			}
			ks.Indexes[indexNameAndMethod] = cols.FieldNames()
			table.AddKey(builder.Index(indexName, cols).Using(method))
		}
	}
}

type ModelScanner struct {
	isScanned bool
	pkg       *packagex.Package

	*types.TypeName
	*ModelScannerConfig
	*Keys
	*builder.Table
	Fields                map[string]*types.Var
	FieldKeyAutoIncrement string
	HasDeletedAt          bool
	HasCreatedAt          bool
	HasUpdatedAt          bool
	HasAutoIncrement      bool
}

func NewModelScanner(pkg *packagex.Package, cfg *ModelScannerConfig) *ModelScanner {
	return &ModelScanner{
		isScanned:          false,
		pkg:                pkg,
		ModelScannerConfig: cfg,
	}
}

func (m *ModelScanner) IsScanned() bool {
	return m.isScanned
}

func (m *ModelScanner) Scan(modelName *types.TypeName, comments string) {
	m.SetDefaults()
	m.TypeName = modelName
	m.Table = builder.T(m.ModelScannerConfig.TableName)
	p := m.pkg.Pkg(modelName.Pkg().Path())

	ForEachStructField(
		modelName.Type().Underlying().Underlying().(*types.Struct),
		func(fieldVar *types.Var, columnName string, tagValue string) {
			col := builder.Col(columnName).Field(fieldVar.Name()).Type("", tagValue)

			for id, o := range p.TypesInfo.Defs {
				if o == fieldVar {
					doc := m.pkg.CommentsOf(id)

					rel, lines := parseColRelFromComment(doc)

					if rel != "" {
						relPath := strings.Split(rel, ".")

						if len(relPath) != 2 {
							continue
						}

						col.Relation = relPath
					}

					if len(lines) > 0 {
						col.Comment = lines[0]
						col.Description = lines
					}
				}
			}

			m.addColumn(col, fieldVar)
		},
	)

	m.HasDeletedAt = m.Table.F(m.FieldKeyDeletedAt) != nil
	m.HasCreatedAt = m.Table.F(m.FieldKeyCreatedAt) != nil
	m.HasUpdatedAt = m.Table.F(m.FieldKeyUpdatedAt) != nil

	keys, lines := parseKeysFromDoc(comments)
	m.Keys = keys

	if len(lines) > 0 {
		m.Description = lines
	}

	if m.HasDeletedAt {
		m.Keys.PatchUniqueIndexesWithSoftDelete(m.FieldKeyDeletedAt)
	}
	m.Keys.Bind(m.Table)

	if autoIncrementCol := m.Table.AutoIncrement(); autoIncrementCol != nil {
		m.HasAutoIncrement = true
		m.FieldKeyAutoIncrement = autoIncrementCol.FieldName
	}

	m.isScanned = true
}

func (m *ModelScanner) addColumn(col *builder.Column, tpe *types.Var) {
	m.Table.Columns.Add(col)
	if m.Fields == nil {
		m.Fields = map[string]*types.Var{}
	}
	m.Fields[col.FieldName] = tpe
}
