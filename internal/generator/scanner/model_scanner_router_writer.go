package scanner

import (
	"gitee.com/eden-framework/codegen"
	str "gitee.com/eden-framework/strings"
)

func (m *ModelScanner) WriteRouters(controllerPkgPath string, errorPkgPath string) map[string]*codegen.File {
	var data = make(map[string]*codegen.File)

	path, file := m.writeRouterRoot()
	data[path] = file

	path, file = m.writeRouterCreateFunc(controllerPkgPath, errorPkgPath)
	data[path] = file

	path, file = m.writeRouterReadAllFunc(controllerPkgPath, errorPkgPath)
	data[path] = file

	if m.FieldKeyUniqueID != "" {
		path, file = m.writeRouterReadOneFunc(controllerPkgPath, errorPkgPath)
		data[path] = file

		path, file = m.writeRouterUpdateFunc(controllerPkgPath, errorPkgPath)
		data[path] = file

		path, file = m.writeRouterDeleteFunc(controllerPkgPath, errorPkgPath)
		data[path] = file
	}

	return data
}

func (m *ModelScanner) writeRouterRoot() (string, *codegen.File) {
	fileName := codegen.GeneratedFileSuffix(str.ToLowerSnakeCase(m.StructName) + "/root.go")
	var file = codegen.NewFile(str.ToLowerSnakeCase(m.StructName), fileName)

	file.WriteBlock(
		codegen.DeclType(
			codegen.Var(
				codegen.Struct(
					codegen.Var(codegen.Type(file.Use("gitee.com/eden-framework/courier", "EmptyOperator"))),
				), "Group",
			),
		),

		codegen.Func().Named("Path").MethodOf(codegen.Var(codegen.Type("Group"))).Return(codegen.Var(codegen.String)).Do(
			codegen.Return(codegen.Val(str.ToLowerSlashCase(str.ToLowerSnakeCase(m.StructName)))),
		),

		codegen.DeclVar(
			codegen.Assign(codegen.Id("Router")).By(
				codegen.Call(file.Use("gitee.com/eden-framework/courier", "NewRouter"), codegen.Expr("Group{}")),
			),
		),
	)

	return fileName, file
}

func (m *ModelScanner) writeRouterCreateFunc(controllerPkgPath string, errorPkgPath string) (string, *codegen.File) {
	fileName := codegen.GeneratedFileSuffix(str.ToLowerSnakeCase(m.StructName) + "/create.go")
	var file = codegen.NewFile(str.ToLowerSnakeCase(m.StructName), fileName)

	file.WriteBlock(
		codegen.DeclType(
			codegen.Var(
				codegen.Struct(
					codegen.Var(codegen.Type(file.Use("gitee.com/eden-framework/courier/httpx", "MethodPost"))),
					codegen.Var(codegen.Type(file.Use(controllerPkgPath, "CreateParams")), "Body").WithTag(`in:"body"`),
				), "Create"+str.ToUpperCamelCase(m.StructName),
			),
		).WithComments("Create" + str.ToUpperCamelCase(m.StructName) + " 创建接口"),
	)

	file.WriteBlock(
		codegen.Func().Named("Path").MethodOf(codegen.Var(codegen.Type("Create" + str.ToUpperCamelCase(m.StructName)))).Return(codegen.Var(codegen.String)).Do(
			codegen.Return(codegen.Val("")),
		),
	)

	var controllerParams = []codegen.Snippet{
		codegen.Expr("req.Body"),
		codegen.Expr("global.Config.MasterDB"),
	}
	if m.WithIDClient != nil && *m.WithIDClient {
		controllerParams = append(controllerParams, codegen.Expr("global.Config.ClientID"))
	}
	file.WriteBlock(
		codegen.Func(
			codegen.Var(codegen.Type(file.Use("context", "Context"))),
		).Named("Output").MethodOf(
			codegen.Var(
				codegen.Type("Create"+str.ToUpperCamelCase(m.StructName)),
				"req",
			),
		).Return(
			codegen.Var(codegen.Interface(), "result"),
			codegen.Var(codegen.Error, "err"),
		).Do(
			codegen.Assign(codegen.Id("result"), codegen.Id("err")).By(
				codegen.Sel(
					codegen.Call(file.Use(controllerPkgPath, "GetRepository")),
					codegen.Call("Create", controllerParams...),
				),
			),
			codegen.If(codegen.Expr("err != nil")).Do(
				codegen.Return(codegen.Nil, codegen.Type(file.Use(errorPkgPath, "InternalError"))),
			),
			codegen.Return(),
		),
	)

	file.WriteBlock(
		codegen.Func().Named("init").Do(
			codegen.Sel(
				codegen.Id("Router"),
				codegen.Call(
					"Register",
					codegen.Call(
						file.Use("gitee.com/eden-framework/courier", "NewRouter"),
						codegen.Expr("Create"+str.ToUpperCamelCase(m.StructName)+"{}"),
					),
				),
			),
		),
	)

	return fileName, file
}

func (m *ModelScanner) writeRouterUpdateFunc(controllerPkgPath string, errorPkgPath string) (string, *codegen.File) {
	fileName := codegen.GeneratedFileSuffix(str.ToLowerSnakeCase(m.StructName) + "/update.go")
	var file = codegen.NewFile(str.ToLowerSnakeCase(m.StructName), fileName)

	field, ok := m.Fields[m.FieldKeyUniqueID]
	if !ok {
		return "", nil
	}

	file.WriteBlock(
		codegen.DeclType(
			codegen.Var(
				codegen.Struct(
					codegen.Var(codegen.Type(file.Use("gitee.com/eden-framework/courier/httpx", "MethodPatch"))),
					codegen.Var(
						getExprFromField(field, field.Type(), file, false),
						"ID",
					).WithTag(`in:"path,string" name:"id"`),
					codegen.Var(codegen.Type(file.Use(controllerPkgPath, "UpdateParams")), "Body").WithTag(`in:"body"`),
				), "Update"+str.ToUpperCamelCase(m.StructName),
			),
		).WithComments("Update" + str.ToUpperCamelCase(m.StructName) + " 更新接口"),
	)

	file.WriteBlock(
		codegen.Func().Named("Path").MethodOf(codegen.Var(codegen.Type("Update" + str.ToUpperCamelCase(m.StructName)))).Return(codegen.Var(codegen.String)).Do(
			codegen.Return(codegen.Val("/:id")),
		),
	)

	file.WriteBlock(
		codegen.Func(
			codegen.Var(codegen.Type(file.Use("context", "Context"))),
		).Named("Output").MethodOf(
			codegen.Var(
				codegen.Type("Update"+str.ToUpperCamelCase(m.StructName)),
				"req",
			),
		).Return(
			codegen.Var(codegen.Interface(), "result"),
			codegen.Var(codegen.Error, "err"),
		).Do(
			codegen.Define(codegen.Id("model"), codegen.Id("err")).By(
				codegen.Sel(
					codegen.Call(file.Use(controllerPkgPath, "GetRepository")),
					codegen.Call(
						"ReadOne",
						codegen.Sel(codegen.Id("req"), codegen.Id("ID")),
						codegen.Expr("global.Config.MasterDB"),
						codegen.Val(true),
					),
				),
			),
			codegen.If(codegen.Expr("err != nil")).Do(
				codegen.Return(codegen.Nil, codegen.Type(file.Use(errorPkgPath, "NotFound"))),
			),
			codegen.Assign(codegen.Id("err")).By(
				codegen.Sel(
					codegen.Call(file.Use(controllerPkgPath, "GetRepository")),
					codegen.Call(
						"Update",
						codegen.Id("model"),
						codegen.Sel(codegen.Id("req"), codegen.Id("Body")),
						codegen.Expr("global.Config.MasterDB"),
					),
				),
			),
			codegen.If(codegen.Expr("err != nil")).Do(
				codegen.Return(codegen.Nil, codegen.Type(file.Use(errorPkgPath, "InternalError"))),
			),
			codegen.Return(),
		),
	)

	file.WriteBlock(
		codegen.Func().Named("init").Do(
			codegen.Sel(
				codegen.Id("Router"),
				codegen.Call(
					"Register",
					codegen.Call(
						file.Use("gitee.com/eden-framework/courier", "NewRouter"),
						codegen.Expr("Update"+str.ToUpperCamelCase(m.StructName)+"{}"),
					),
				),
			),
		),
	)

	return fileName, file
}

func (m *ModelScanner) writeRouterDeleteFunc(controllerPkgPath string, errorPkgPath string) (string, *codegen.File) {
	fileName := codegen.GeneratedFileSuffix(str.ToLowerSnakeCase(m.StructName) + "/delete.go")
	var file = codegen.NewFile(str.ToLowerSnakeCase(m.StructName), fileName)

	field, ok := m.Fields[m.FieldKeyUniqueID]
	if !ok {
		return "", nil
	}

	file.WriteBlock(
		codegen.DeclType(
			codegen.Var(
				codegen.Struct(
					codegen.Var(codegen.Type(file.Use("gitee.com/eden-framework/courier/httpx", "MethodDelete"))),
					codegen.Var(
						getExprFromField(field, field.Type(), file, false),
						"ID",
					).WithTag(`in:"path,string" name:"id"`),
				), "Delete"+str.ToUpperCamelCase(m.StructName),
			),
		).WithComments("Delete" + str.ToUpperCamelCase(m.StructName) + " 删除接口"),
	)

	file.WriteBlock(
		codegen.Func().Named("Path").MethodOf(codegen.Var(codegen.Type("Delete" + str.ToUpperCamelCase(m.StructName)))).Return(codegen.Var(codegen.String)).Do(
			codegen.Return(codegen.Val("/:id")),
		),
	)

	file.WriteBlock(
		codegen.Func(
			codegen.Var(codegen.Type(file.Use("context", "Context"))),
		).Named("Output").MethodOf(
			codegen.Var(
				codegen.Type("Delete"+str.ToUpperCamelCase(m.StructName)),
				"req",
			),
		).Return(
			codegen.Var(codegen.Interface(), "result"),
			codegen.Var(codegen.Error, "err"),
		).Do(
			codegen.Assign(codegen.Id("err")).By(
				codegen.Sel(
					codegen.Call(file.Use(controllerPkgPath, "GetRepository")),
					codegen.Call(
						"Delete",
						codegen.Sel(codegen.Id("req"), codegen.Id("ID")),
						codegen.Expr("global.Config.MasterDB"),
					),
				),
			),
			codegen.If(codegen.Expr("err != nil")).Do(
				codegen.Return(codegen.Nil, codegen.Type(file.Use(errorPkgPath, "InternalError"))),
			),
			codegen.Return(),
		),
	)

	file.WriteBlock(
		codegen.Func().Named("init").Do(
			codegen.Sel(
				codegen.Id("Router"),
				codegen.Call(
					"Register",
					codegen.Call(
						file.Use("gitee.com/eden-framework/courier", "NewRouter"),
						codegen.Expr("Delete"+str.ToUpperCamelCase(m.StructName)+"{}"),
					),
				),
			),
		),
	)

	return fileName, file
}

func (m *ModelScanner) writeRouterReadAllFunc(controllerPkgPath string, errorPkgPath string) (string, *codegen.File) {
	fileName := codegen.GeneratedFileSuffix(str.ToLowerSnakeCase(m.StructName) + "/read_all.go")
	var file = codegen.NewFile(str.ToLowerSnakeCase(m.StructName), fileName)

	file.WriteBlock(
		codegen.DeclType(
			codegen.Var(
				codegen.Struct(
					codegen.Var(codegen.Type(file.Use("gitee.com/eden-framework/courier/httpx", "MethodGet"))),
					codegen.Var(codegen.Type(file.Use(controllerPkgPath, "ReadAllParams"))),
				), "ReadAll"+str.ToUpperCamelCase(m.StructName),
			),
		).WithComments("ReadAll" + str.ToUpperCamelCase(m.StructName) + " 批量读取接口"),
	)

	file.WriteBlock(
		codegen.Func().Named("Path").MethodOf(codegen.Var(codegen.Type("ReadAll" + str.ToUpperCamelCase(m.StructName)))).Return(codegen.Var(codegen.String)).Do(
			codegen.Return(codegen.Val("")),
		),
	)

	file.WriteBlock(
		codegen.DeclType(
			codegen.Var(
				codegen.Struct(
					codegen.Var(
						codegen.Slice(codegen.Type(file.UseExposed(m.Type().String()))),
						"Data",
					).WithTag(`json:"data"`),
					codegen.Var(codegen.Int, "Total").WithTag(`json:"total"`),
				), "ReadAllResp",
			),
		),
	)

	file.WriteBlock(
		codegen.Func(
			codegen.Var(codegen.Type(file.Use("context", "Context"))),
		).Named("Output").MethodOf(
			codegen.Var(
				codegen.Type("ReadAll"+str.ToUpperCamelCase(m.StructName)),
				"req",
			),
		).Return(
			codegen.Var(codegen.Interface(), "result"),
			codegen.Var(codegen.Error, "err"),
		).Do(
			codegen.Define(codegen.Id("data"), codegen.Id("count"), codegen.Id("err")).By(
				codegen.Sel(
					codegen.Call(file.Use(controllerPkgPath, "GetRepository")),
					codegen.Call(
						"ReadAll",
						codegen.Sel(codegen.Id("req"), codegen.Id("ReadAllParams")),
						codegen.Val(true),
					),
				),
			),
			codegen.If(codegen.Expr("err != nil")).Do(
				codegen.Return(codegen.Nil, codegen.Type(file.Use(errorPkgPath, "InternalError"))),
			),
			codegen.Return(
				codegen.Expr(
					`&ReadAllResp{
	Data:  data,
	Total: count,
}`,
				), codegen.Nil,
			),
		),
	)

	file.WriteBlock(
		codegen.Func().Named("init").Do(
			codegen.Sel(
				codegen.Id("Router"),
				codegen.Call(
					"Register",
					codegen.Call(
						file.Use("gitee.com/eden-framework/courier", "NewRouter"),
						codegen.Expr("ReadAll"+str.ToUpperCamelCase(m.StructName)+"{}"),
					),
				),
			),
		),
	)

	return fileName, file
}

func (m *ModelScanner) writeRouterReadOneFunc(controllerPkgPath string, errorPkgPath string) (string, *codegen.File) {
	fileName := codegen.GeneratedFileSuffix(str.ToLowerSnakeCase(m.StructName) + "/read_one.go")
	var file = codegen.NewFile(str.ToLowerSnakeCase(m.StructName), fileName)

	field, ok := m.Fields[m.FieldKeyUniqueID]
	if !ok {
		return "", nil
	}

	file.WriteBlock(
		codegen.DeclType(
			codegen.Var(
				codegen.Struct(
					codegen.Var(codegen.Type(file.Use("gitee.com/eden-framework/courier/httpx", "MethodGet"))),
					codegen.Var(
						getExprFromField(field, field.Type(), file, false),
						"ID",
					).WithTag(`in:"path,string" name:"id"`),
				), "ReadOne"+str.ToUpperCamelCase(m.StructName),
			),
		).WithComments("ReadOne" + str.ToUpperCamelCase(m.StructName) + " 获取接口"),
	)

	file.WriteBlock(
		codegen.Func().Named("Path").MethodOf(codegen.Var(codegen.Type("ReadOne" + str.ToUpperCamelCase(m.StructName)))).Return(codegen.Var(codegen.String)).Do(
			codegen.Return(codegen.Val("/:id")),
		),
	)

	file.WriteBlock(
		codegen.Func(
			codegen.Var(codegen.Type(file.Use("context", "Context"))),
		).Named("Output").MethodOf(
			codegen.Var(
				codegen.Type("ReadOne"+str.ToUpperCamelCase(m.StructName)),
				"req",
			),
		).Return(
			codegen.Var(codegen.Interface(), "result"),
			codegen.Var(codegen.Error, "err"),
		).Do(
			codegen.Assign(codegen.Id("result"), codegen.Id("err")).By(
				codegen.Sel(
					codegen.Call(file.Use(controllerPkgPath, "GetRepository")),
					codegen.Call(
						"ReadOne",
						codegen.Sel(codegen.Id("req"), codegen.Id("ID")),
						codegen.Expr("global.Config.MasterDB"),
						codegen.Val(false),
					),
				),
			),
			codegen.If(codegen.Expr("err != nil")).Do(
				codegen.Return(codegen.Nil, codegen.Type(file.Use(errorPkgPath, "NotFound"))),
			),
			codegen.Return(),
		),
	)

	file.WriteBlock(
		codegen.Func().Named("init").Do(
			codegen.Sel(
				codegen.Id("Router"),
				codegen.Call(
					"Register",
					codegen.Call(
						file.Use("gitee.com/eden-framework/courier", "NewRouter"),
						codegen.Expr("ReadOne"+str.ToUpperCamelCase(m.StructName)+"{}"),
					),
				),
			),
		),
	)

	return fileName, file
}
