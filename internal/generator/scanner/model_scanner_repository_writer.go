package scanner

import (
	"fmt"
	"go/types"

	"gitee.com/eden-framework/codegen"
	"gitee.com/eden-framework/eden-framework/internal/generator/importer"
	"gitee.com/eden-framework/sqlx/builder"
	str "gitee.com/eden-framework/strings"
)

func (m *ModelScanner) WriteRepository(repository *codegen.File, parameter *codegen.File) {
	m.writeRepositoryHeader(repository)

	m.writeCreateParameter(parameter)
	m.writeCreateFunc(repository)

	m.writeReadAllParameter(parameter)
	m.writeReadAllFunc(repository)

	if m.FieldKeyUniqueID != "" {
		m.writeReadOneFunc(repository)

		m.writeUpdateParameter(parameter)
		m.writeUpdateFunc(repository)

		m.writeDeleteFunc(repository)
		m.writeSoftDeleteFunc(repository)
	}

	m.writeTxFunc(repository)
}

func (m *ModelScanner) writeRepositoryHeader(repository *codegen.File) {
	repository.WriteBlock(
		codegen.DeclVar(
			codegen.Var(codegen.Star(codegen.Type("Repository")), "repository"),
		),
	)

	repository.WriteBlock(
		codegen.Func().
			Named("GetRepository").
			Return(codegen.Var(codegen.Star(codegen.Type("Repository")))).
			Do(
				codegen.If(codegen.Expr(`repository == nil`)).Do(
					codegen.Expr(`repository = &Repository{isInit: false}`),
				),
				codegen.Return(codegen.Expr(`repository`)),
			),
	)

	repository.WriteBlock(
		codegen.DeclType(
			codegen.Var(
				codegen.Struct(
					codegen.Var(codegen.Bool, "isInit"),
					codegen.Var(codegen.Type(repository.Use("gitee.com/eden-framework/sqlx", "DBExecutor")), "db"),
				),
				"Repository",
			),
		),
	)

	repository.WriteBlock(
		codegen.Func(
			codegen.Var(codegen.Type(repository.Use("gitee.com/eden-framework/sqlx", "DBExecutor")), "db"),
		).Named("Init").MethodOf(
			codegen.Var(
				codegen.Star(codegen.Type("Repository")),
				"r",
			),
		).Do(
			codegen.Expr(`r.db = db`),
			codegen.Expr(`r.isInit = true`),
		),
	)
}

func (m *ModelScanner) writeCreateParameter(parameter *codegen.File) {
	createParamsStruct := codegen.Struct()
	assignSnippets := make([]codegen.Snippet, 0)
	for key, field := range m.Fields {
		if m.Keys != nil {
			if hasPrimary(key, m.Keys.Primary) {
				continue
			}
		}
		if key == m.FieldKeyUniqueID {
			continue
		}
		if m.HasCreatedAt && key == m.FieldKeyCreatedAt {
			continue
		}
		if m.HasUpdatedAt && key == m.FieldKeyUpdatedAt {
			continue
		}
		if m.HasDeletedAt && key == m.FieldKeyDeletedAt {
			continue
		}

		appendStructField(
			parameter, m.Table, field, false, &createParamsStruct.Fields, func(field *types.Var) map[string][]string {
				return map[string][]string{
					"json": {str.ToLowerCamelCase(field.Name())},
				}
			},
		)

		assignSnippets = append(
			assignSnippets,
			codegen.KeyValue(codegen.Id(field.Name()), codegen.Sel(codegen.Id("p"), codegen.Id(field.Name()))),
		)
	}
	parameter.WriteBlock(
		codegen.DeclType(
			codegen.Var(createParamsStruct, "CreateParams"),
		),

		codegen.Func().Named("Model").MethodOf(
			codegen.Var(
				codegen.Type("CreateParams"),
				"p",
			),
		).Return(codegen.Var(codegen.Star(codegen.Type(parameter.UseExposed(m.Type().String()))))).Do(
			codegen.Define(codegen.Id("model")).By(
				codegen.Compose(
					codegen.Type(parameter.UseExposed(m.Type().String())),
					assignSnippets...,
				),
			),
			codegen.Return(codegen.Unary(codegen.Id("model"))),
		),
	)
}

func (m *ModelScanner) writeCreateFunc(repository *codegen.File) {
	funcBlock := codegen.Func(
		codegen.Var(codegen.Type("CreateParams"), "params"),
		codegen.Var(codegen.Type(repository.Use("gitee.com/eden-framework/sqlx", "DBExecutor")), "db"),
	).Named("Create").MethodOf(codegen.Var(codegen.Star(codegen.Type("Repository")), "r")).Return(
		codegen.Var(codegen.Star(codegen.Type(repository.UseExposed(m.Type().String())))),
		codegen.Var(codegen.Error),
	).Do(
		initCheckExpr(m.StructName, repository),
		dbCheckExpr(),
		codegen.Expr(
			`
model := params.Model()
`,
		),
	)

	if m.WithIDClient != nil && *m.WithIDClient {
		funcBlock.Params = append(
			funcBlock.Params,
			codegen.Var(
				codegen.Type(
					repository.Use(
						"gitee.com/eden-framework/client/client_srv_id",
						"ClientSrvIDInterface",
					),
				), "clientID",
			),
		)
		funcBlock.Body = append(
			funcBlock.Body, codegen.Expr(
				`
resp, err := clientID.GenerateID()
if err != nil {
	return nil, err
}
model.? = resp.Body.ID

err = model.Create(db)`, codegen.Id(m.FieldKeyUniqueID),
			),
		)
	} else {
		funcBlock.Body = append(
			funcBlock.Body, codegen.Expr(`err := model.Create(db)`),
		)
	}
	funcBlock.Body = append(
		funcBlock.Body, codegen.Expr(
			`if err != nil {
	errFmt := fmt.Errorf("[?Repository.Create] model.Create(db) err: %v, params: %+v", err, params)
	?
	return nil, errFmt
}
return model, nil`,
			codegen.Id(m.StructName),
			logError(repository, codegen.Id("errFmt")),
		),
	)
	repository.WriteBlock(funcBlock)
}

func (m *ModelScanner) writeReadAllParameter(parameter *codegen.File) {
	readAllParamStruct := codegen.Struct()
	conditionIfStmtSnippets := make([]codegen.Snippet, 0)
	conditionIfStmtSnippets = append(
		conditionIfStmtSnippets, codegen.DeclVar(
			codegen.Var(
				codegen.Type(
					parameter.Use(
						"gitee.com/eden-framework/sqlx/builder",
						"SqlCondition",
					),
				), "condition",
			),
		),
	)

	if m.Keys.Indexes != nil && len(m.Keys.Indexes) > 0 {
		conditionIfStmtSnippets = append(
			conditionIfStmtSnippets, codegen.DeclVar(
				codegen.Assign(codegen.Id("model")).By(
					codegen.Compose(codegen.Type(parameter.UseExposed(m.Type().String()))),
				),
			),
		)
	}
	for key, field := range m.Fields {
		if m.Keys != nil {
			if hasIndex(key, m.Keys.Indexes) {
				appendStructField(
					parameter,
					m.Table,
					field,
					false,
					&readAllParamStruct.Fields,
					func(field *types.Var) map[string][]string {
						return map[string][]string{
							"json":    {str.ToLowerCamelCase(field.Name())},
							"in":      {"query"},
							"default": {""},
						}
					},
				)
				appendConditionIfStmt(parameter, field, &conditionIfStmtSnippets)
			}
		}
	}
	conditionIfStmtSnippets = append(conditionIfStmtSnippets, codegen.Return(codegen.Id("condition")))
	if m.WithReadPagination != nil && *m.WithReadPagination {
		appendPaginationField(&readAllParamStruct.Fields)
	}

	parameter.WriteBlock(codegen.DeclType(codegen.Var(readAllParamStruct, "ReadAllParams")))

	parameter.WriteBlock(
		codegen.Func().Named("Conditions").MethodOf(
			codegen.Var(
				codegen.Type("ReadAllParams"),
				"p",
			),
		).Return(codegen.Var(codegen.Type(parameter.Use("gitee.com/eden-framework/sqlx/builder", "SqlCondition")))).Do(
			conditionIfStmtSnippets...,
		),
	)

	if m.WithReadPagination != nil && *m.WithReadPagination {
		var sortExpr codegen.Snippet
		if m.HasCreatedAt {
			sortExpr = codegen.Expr(
				"additions = append(additions, ?)",
				codegen.Call(
					parameter.Use("gitee.com/eden-framework/sqlx/builder", "OrderBy"),
					codegen.Call(
						parameter.Use("gitee.com/eden-framework/sqlx/builder", "DescOrder"),
						codegen.Expr(
							"(&?).?",
							codegen.Compose(codegen.Type(parameter.UseExposed(m.Type().String()))),
							codegen.Call("FieldCreatedAt"),
						),
					),
				),
			)
		}
		parameter.WriteBlock(
			codegen.Func().Named("Additions").MethodOf(
				codegen.Var(
					codegen.Type("ReadAllParams"),
					"p",
				),
			).Return(
				codegen.Var(
					codegen.Slice(
						codegen.Type(
							parameter.Use(
								"gitee.com/eden-framework/sqlx/builder",
								"Addition",
							),
						),
					),
				),
			).Do(
				codegen.DeclVar(
					codegen.Assign(codegen.Id("additions")).By(
						codegen.Call(
							"make", codegen.Slice(
								codegen.Type(
									parameter.Use(
										"gitee.com/eden-framework/sqlx/builder",
										"Addition",
									),
								),
							), codegen.Id("0"),
						),
					),
				),
				codegen.If(codegen.Expr("? == 0", codegen.Sel(codegen.Id("p"), codegen.Id("Size")))).Do(
					codegen.Expr("? = 10", codegen.Sel(codegen.Id("p"), codegen.Id("Size"))),
				),
				codegen.If(
					codegen.Expr(
						"? > 0",
						codegen.Sel(codegen.Id("p"), codegen.Id("Size")),
					),
				).Do(
					codegen.Expr(
						"? := ?",
						codegen.Id("limit"),
						codegen.Call(
							parameter.Use("gitee.com/eden-framework/sqlx/builder", "Limit"),
							codegen.Expr("int64(p.Size)"),
						),
					),
					codegen.If(codegen.Expr("? > 0", codegen.Sel(codegen.Id("p"), codegen.Id("Offset")))).Do(
						codegen.Expr("? = ?", codegen.Id("limit"), codegen.Expr("limit.Offset(int64(p.Offset))")),
					),
					codegen.Expr("additions = append(additions, limit)"),
				),
				sortExpr,
				codegen.Return(codegen.Expr("additions")),
			),
		)
	}
}

func (m *ModelScanner) writeReadAllFunc(repository *codegen.File) {
	repository.WriteBlock(
		codegen.Func(
			codegen.Var(codegen.Type("ReadAllParams"), "params"),
			codegen.Var(codegen.Bool, "withCount"),
		).Named("ReadAll").MethodOf(codegen.Var(codegen.Star(codegen.Type("Repository")), "r")).Return(
			codegen.Var(codegen.Slice(codegen.Type(repository.UseExposed(m.Type().String()))), "data"),
			codegen.Var(codegen.Int, "count"),
			codegen.Var(codegen.Error, "err"),
		).Do(
			initCheckExpr(m.StructName, repository),
			codegen.Expr(
				`
model := &?{}
data, err = model.List(r.db, params.Conditions(), params.Additions()...)
if err != nil {
	errFmt := fmt.Errorf("[?Flows] model.List err: %v, params: %+v", err, params)
	?
	return nil, 0, errFmt
}
if withCount {
	count, err = model.Count(r.db, params.Conditions())
	if err != nil {
		errFmt := fmt.Errorf("[?Flows] model.Count err: %v, params: %+v", err, params)
		?
		return nil, 0, errFmt
	}
}

return`,
				codegen.Type(repository.UseExposed(m.Type().String())),
				codegen.Id(m.StructName),
				logError(repository, codegen.Id("errFmt")),
				codegen.Id(m.StructName),
				logError(repository, codegen.Id("errFmt")),
			),
		),
	)
}

func (m *ModelScanner) writeReadOneFunc(repository *codegen.File) {
	field, ok := m.Fields[m.FieldKeyUniqueID]
	if !ok {
		return
	}

	repository.WriteBlock(
		codegen.Func(
			codegen.Var(getExprFromField(field, field.Type(), repository, false), "id"),
			codegen.Var(codegen.Type(repository.Use("gitee.com/eden-framework/sqlx", "DBExecutor")), "db"),
			codegen.Var(codegen.Bool, "forUpdate"),
		).Named("ReadOne").MethodOf(codegen.Var(codegen.Star(codegen.Type("Repository")), "r")).Return(
			codegen.Var(codegen.Star(codegen.Type(repository.UseExposed(m.Type().String())))),
			codegen.Var(codegen.Error),
		).Do(
			initCheckExpr(m.StructName, repository),
			dbCheckExpr(),
			codegen.Expr(
				`
var err error
model := ?
if forUpdate {
	err = ?
} else {
	err = ?
}
if err != nil {
	errFmt := fmt.Errorf("[?Repository][ReadOne] err: %v, id: %v", err, id)
	?
	return nil, errFmt
}
return &model, nil`,
				codegen.Compose(
					codegen.Type(
						repository.UseExposed(m.Type().String()),
					),
					codegen.KeyValue(codegen.Id(m.FieldKeyUniqueID), codegen.Id("id")),
				),
				codegen.Call(fmt.Sprintf("model.FetchBy%sForUpdate", m.FieldKeyUniqueID), codegen.Id("db")),
				codegen.Call(fmt.Sprintf("model.FetchBy%s", m.FieldKeyUniqueID), codegen.Id("db")),
				codegen.Id(m.StructName),
				logError(repository, codegen.Id("errFmt")),
			),
		),
	)
}

func (m *ModelScanner) writeUpdateParameter(parameter *codegen.File) {
	updateParamsStruct := codegen.Struct()
	fillIfStmtSnippets := make([]codegen.Snippet, 0)
	fillIfStmtSnippets = append(
		fillIfStmtSnippets, codegen.Assign(codegen.Id("zeroFields")).By(
			codegen.Call("make", codegen.Slice(codegen.String), codegen.Expr("?", 0)),
		),
	)
	fillIfStmtSnippets = append(
		fillIfStmtSnippets, codegen.Expr(
			`if model == nil {
	return
}`,
		),
	)

	for key, field := range m.Fields {
		if m.Keys != nil {
			if hasPrimary(key, m.Keys.Primary) {
				continue
			}
		}
		if key == m.FieldKeyUniqueID {
			continue
		}
		if m.HasCreatedAt && key == m.FieldKeyCreatedAt {
			continue
		}
		if m.HasUpdatedAt && key == m.FieldKeyUpdatedAt {
			continue
		}
		if m.HasDeletedAt && key == m.FieldKeyDeletedAt {
			continue
		}

		appendStructField(
			parameter, m.Table, field, true, &updateParamsStruct.Fields, func(field *types.Var) map[string][]string {
				return map[string][]string{
					"json":    {str.ToLowerCamelCase(field.Name())},
					"default": {""},
				}
			},
		)

		appendFillIfStmt(parameter, field, &fillIfStmtSnippets)
	}
	fillIfStmtSnippets = append(fillIfStmtSnippets, codegen.Return())

	parameter.WriteBlock(
		codegen.DeclType(
			codegen.Var(updateParamsStruct, "UpdateParams"),
		),
	)
	parameter.WriteBlock(
		codegen.Func(
			codegen.Var(
				codegen.Star(codegen.Type(parameter.UseExposed(m.Type().String()))),
				"model",
			),
		).Named("Fill").MethodOf(
			codegen.Var(
				codegen.Star(codegen.Type("UpdateParams")),
				"p",
			),
		).Return(codegen.Var(codegen.Slice(codegen.String), "zeroFields")).Do(
			fillIfStmtSnippets...,
		),
	)
}

func (m *ModelScanner) writeUpdateFunc(repository *codegen.File) {
	repository.WriteBlock(
		codegen.Func(
			codegen.Var(codegen.Star(codegen.Type(repository.UseExposed(m.Type().String()))), "model"),
			codegen.Var(codegen.Type("UpdateParams"), "params"),
			codegen.Var(codegen.Type(repository.Use("gitee.com/eden-framework/sqlx", "DBExecutor")), "db"),
		).Named("Update").MethodOf(
			codegen.Var(
				codegen.Star(codegen.Type("Repository")),
				"r",
			),
		).Return(codegen.Var(codegen.Error)).Do(
			initCheckExpr(m.StructName, repository),
			dbCheckExpr(),
			codegen.If(codegen.Expr("model == nil")).Do(
				codegen.Expr(
					`errFmt := fmt.Errorf(?)`,
					fmt.Sprintf("[%sRepository][Update] model == nil", m.StructName),
				),
				logError(repository, codegen.Id("errFmt")),
				codegen.Return(codegen.Id("errFmt")),
			),
			codegen.Define(codegen.Id("zeroFields")).By(
				codegen.Sel(
					codegen.Id("params"),
					codegen.Call("Fill", codegen.Id("model")),
				),
			),
			codegen.Define(codegen.Id("err")).By(
				codegen.Sel(
					codegen.Id("model"),
					codegen.Call(
						fmt.Sprintf("UpdateBy%sWithStruct", m.FieldKeyUniqueID),
						codegen.Id("db"),
						codegen.Id("zeroFields"),
					).WithEllipsis(),
				),
			),
			codegen.If(codegen.Expr("err != nil")).Do(
				codegen.Expr(
					`errFmt := fmt.Errorf("[?Repository][Update] model.UpdateBy?WithStruct(db) err: %v, params: %+v", ?, ?)`,
					codegen.Id(m.StructName),
					codegen.Id(m.FieldKeyUniqueID),
					codegen.Id("err"),
					codegen.Id("params"),
				),
				logError(repository, codegen.Id("errFmt")),
				codegen.Return(codegen.Id("errFmt")),
			),
			codegen.Return(codegen.Nil),
		),
	)
}

func (m *ModelScanner) writeDeleteFunc(repository *codegen.File) {
	field, ok := m.Fields[m.FieldKeyUniqueID]
	if !ok {
		return
	}

	repository.WriteBlock(
		codegen.Func(
			codegen.Var(getExprFromField(field, field.Type(), repository, false), "id"),
			codegen.Var(codegen.Type(repository.Use("gitee.com/eden-framework/sqlx", "DBExecutor")), "db"),
		).Named("Delete").MethodOf(codegen.Var(codegen.Star(codegen.Type("Repository")), "r")).Return(
			codegen.Var(codegen.Error),
		).Do(
			initCheckExpr(m.StructName, repository),
			dbCheckExpr(),
			codegen.Define(codegen.Id("model")).By(
				codegen.Compose(
					codegen.Type(repository.UseExposed(m.Type().String())),
					codegen.KeyValue(codegen.Id(m.FieldKeyUniqueID), codegen.Id("id")),
				),
			),
			codegen.Define(codegen.Id("err")).By(
				codegen.Sel(
					codegen.Id("model"),
					codegen.Call(fmt.Sprintf("DeleteBy%s", m.FieldKeyUniqueID), codegen.Id("db")),
				),
			),
			codegen.If(codegen.Expr("err != nil")).Do(
				codegen.Expr(
					`errFmt := fmt.Errorf("[?Repository][Delete] model.DeleteBy?(db) err: %v, id: %v", ?, ?)`,
					codegen.Id(m.StructName),
					codegen.Id(m.FieldKeyUniqueID),
					codegen.Id("err"),
					codegen.Id("id"),
				),
				logError(repository, codegen.Id("errFmt")),
				codegen.Return(codegen.Id("errFmt")),
			),
			codegen.Return(codegen.Nil),
		),
	)
}

func (m *ModelScanner) writeSoftDeleteFunc(repository *codegen.File) {
	field, ok := m.Fields[m.FieldKeyUniqueID]
	if !ok {
		return
	}

	repository.WriteBlock(
		codegen.Func(
			codegen.Var(getExprFromField(field, field.Type(), repository, false), "id"),
			codegen.Var(codegen.Type(repository.Use("gitee.com/eden-framework/sqlx", "DBExecutor")), "db"),
		).Named("SoftDelete").MethodOf(codegen.Var(codegen.Star(codegen.Type("Repository")), "r")).Return(
			codegen.Var(codegen.Error),
		).Do(
			initCheckExpr(m.StructName, repository),
			dbCheckExpr(),
			codegen.Define(codegen.Id("model")).By(
				codegen.Compose(
					codegen.Type(repository.UseExposed(m.Type().String())),
					codegen.KeyValue(codegen.Id(m.FieldKeyUniqueID), codegen.Id("id")),
				),
			),
			codegen.Define(codegen.Id("err")).By(
				codegen.Sel(
					codegen.Id("model"),
					codegen.Call(fmt.Sprintf("SoftDeleteBy%s", m.FieldKeyUniqueID), codegen.Id("db")),
				),
			),
			codegen.If(codegen.Expr("err != nil")).Do(
				codegen.Expr(
					`errFmt := fmt.Errorf("[?Repository][SoftDelete] model.SoftDeleteBy?(db) err: %v, id: %v", ?, ?)`,
					codegen.Id(m.StructName),
					codegen.Id(m.FieldKeyUniqueID),
					codegen.Id("err"),
					codegen.Id("id"),
				),
				logError(repository, codegen.Id("errFmt")),
				codegen.Return(codegen.Id("errFmt")),
			),
			codegen.Return(codegen.Nil),
		),
	)
}

func (m *ModelScanner) writeTxFunc(repository *codegen.File) {
	repository.WriteBlock(
		codegen.Func(
			codegen.Var(codegen.Type(repository.Use("gitee.com/eden-framework/sqlx", "Task")), "task"),
		).Named("Tx").MethodOf(codegen.Var(codegen.Star(codegen.Type("Repository")), "r")).Return(
			codegen.Var(codegen.Error),
		).Do(
			codegen.Expr(
				"? := ?",
				codegen.Id("tx"),
				codegen.Call(
					repository.Use("gitee.com/eden-framework/sqlx", "NewTasks"),
					codegen.Sel(codegen.Id("r"), codegen.Id("db")),
				),
			),
			codegen.Expr(
				"? := ?",
				codegen.Id("err"),
				codegen.Call(
					"tx.With(task).Do",
				),
			),
			codegen.Expr(
				`
	if err != nil {
		errFmt := fmt.Errorf(
			"[[CategoryRepository][Tx] tx err: %v",
			err,
		)
		?
	}
`,
				logError(repository, codegen.Id("errFmt")),
			),
			codegen.Return(codegen.Id("err")),
		),
	)
}

func hasPrimary(target string, haystack []string) bool {
	for _, needle := range haystack {
		if target == needle {
			return true
		}
	}
	return false
}

func hasIndex(target string, haystack builder.Indexes) bool {
	for _, val := range haystack {
		for _, needle := range val {
			if target == needle {
				return true
			}
		}
	}
	return false
}

func appendConditionIfStmt(file *codegen.File, field *types.Var, snippets *[]codegen.Snippet) {
	*snippets = append(
		*snippets,
		codegen.If(
			codegen.Expr(
				"? != ?",
				codegen.Sel(codegen.Id("p"), codegen.Id(field.Name())),
				toNilVal(field.Type()),
			),
		).Do(
			codegen.Assign(codegen.Id("condition")).By(
				codegen.Call(
					file.Use("gitee.com/eden-framework/sqlx/builder", "And"),
					codegen.Id("condition"),
					codegen.Sel(
						codegen.Sel(codegen.Id("model"), codegen.Call("Field"+field.Name())),
						codegen.Call("Eq", codegen.Sel(codegen.Id("p"), codegen.Id(field.Name()))),
					),
				),
			),
		),
	)
}

func appendFillIfStmt(file *codegen.File, field *types.Var, snippets *[]codegen.Snippet) {
	if isBasicType(field.Type()) {
		*snippets = append(
			*snippets,
			codegen.If(
				codegen.Expr(
					"? != ?",
					codegen.Sel(codegen.Id("p"), codegen.Id(field.Name())),
					nil,
				),
			).Do(
				codegen.Expr(
					`? = ?`,
					codegen.Sel(codegen.Id("model"), codegen.Id(field.Name())),
					codegen.Expr(`*?`, codegen.Sel(codegen.Id("p"), codegen.Id(field.Name()))),
				),
				codegen.Expr(
					`? = append(?, ?)`,
					codegen.Id("zeroFields"),
					codegen.Id("zeroFields"),
					codegen.Sel(codegen.Id("model"), codegen.Call("FieldKey"+field.Name())),
				),
			),
		)
	} else {
		*snippets = append(
			*snippets,
			codegen.If(
				codegen.Expr(
					"? != ?",
					codegen.Sel(codegen.Id("p"), codegen.Id(field.Name())),
					toNilVal(field.Type()),
				),
			).Do(
				codegen.Expr(
					`? = ?`,
					codegen.Sel(codegen.Id("model"), codegen.Id(field.Name())),
					codegen.Expr(`?`, codegen.Sel(codegen.Id("p"), codegen.Id(field.Name()))),
				),
			),
		)
	}
}

func appendStructField(
	file *codegen.File,
	table *builder.Table,
	field *types.Var,
	withBasicStar bool,
	fields *[]*codegen.SnippetField,
	tagFunc func(field *types.Var) map[string][]string,
) {
	if file == nil {
		panic("[appendStructField] file must not be nil")
	}
	if field == nil {
		panic("[appendStructField] field must not be nil")
	}
	if fields == nil {
		panic("[appendStructField] fields must not be nil")
	}
	if tagFunc == nil {
		panic("[appendStructField] tagFunc must not be nil")
	}

	var comment string
	if table != nil {
		col := table.F(field.Name())
		if col != nil {
			comment = col.Comment
		}
	}

	*fields = append(
		*fields,
		codegen.Var(
			getExprFromField(field, field.Type(), file, withBasicStar),
			field.Name(),
		).WithTags(tagFunc(field)).WithComments(comment),
	)
}

func getExprFromField(field *types.Var, typ types.Type, file *codegen.File, withBasicStar bool) codegen.SnippetType {
	var pkgPath, exposedType = importer.GetPackagePathAndDecl(typ.String())

	switch t := typ.(type) {
	case *types.Basic:
		if withBasicStar {
			return codegen.Star(codegen.BuiltInType(pkgPath))
		} else {
			return codegen.BuiltInType(pkgPath)
		}
	// case *types.Named:
	// 	return getExprFromField(field, typ.Underlying(), file, withBasicStar)
	case *types.Slice:
		return codegen.Slice(getExprFromField(field, t.Elem(), file, false))
	case *types.Map:
		return codegen.Map(
			getExprFromField(field, t.Key(), file, false),
			getExprFromField(field, t.Elem(), file, false),
		)
	case *types.Pointer:
		return codegen.Star(getExprFromField(field, t.Elem(), file, false))
	case *types.Named, *types.Struct:
		return codegen.Type(file.Use(pkgPath, exposedType))
	}
	return codegen.Interface()
}

func appendPaginationField(fields *[]*codegen.SnippetField) {
	*fields = append(
		*fields, codegen.Var(codegen.Int32, "Size").WithTags(
			map[string][]string{
				"in":       {"query"},
				"default":  {"10"},
				"name":     {"size", "omitempty"},
				"validate": {"@int32[-1,]"},
			},
		).WithComments("分页大小"),
	)
	*fields = append(
		*fields, codegen.Var(codegen.Int32, "Offset").WithTags(
			map[string][]string{
				"in":       {"query"},
				"default":  {"0"},
				"name":     {"offset", "omitempty"},
				"validate": {"@int32[0,]"},
			},
		).WithComments("偏移量"),
	)
}

func initCheckExpr(name string, file *codegen.File) codegen.Snippet {
	return codegen.If(codegen.Expr("!r.isInit")).Do(
		logPanic(file, fmt.Sprintf("[%sRepository] not Init", name)),
	)
}

func dbCheckExpr() codegen.Snippet {
	return codegen.Expr(
		`if db == nil {
	db = r.db
}`,
	)
}

func argsExpr(args []interface{}) []codegen.Snippet {
	var exprs = make([]codegen.Snippet, 0)
	for _, arg := range args {
		if v, ok := arg.(codegen.Snippet); ok {
			exprs = append(exprs, v)
		} else {
			exprs = append(exprs, codegen.Expr("?", arg))
		}
	}
	return exprs
}

func logPanic(file *codegen.File, args ...interface{}) codegen.Snippet {
	return codegen.Call(
		file.Use("github.com/sirupsen/logrus", "Panic"),
		argsExpr(args)...,
	)
}

func logError(file *codegen.File, args ...interface{}) codegen.Snippet {
	return codegen.Call(
		file.Use("github.com/sirupsen/logrus", "Error"),
		argsExpr(args)...,
	)
}
