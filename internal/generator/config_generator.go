package generator

import (
	"gitee.com/eden-framework/eden-framework/internal/generator/files"
	"gitee.com/eden-framework/envconfig"
	"path"
)

type ConfigGenerator struct {
	envVars  []envconfig.EnvVar
	fileName string
}

func (c ConfigGenerator) Load(path string) {
}

func (c ConfigGenerator) Pick() {
}

func (c ConfigGenerator) Output(outputPath string) Outputs {
	outputs := Outputs{}

	configDefaultFile := files.NewConfigDefaultFile(c.envVars)
	outputs.Add(path.Join(outputPath, c.fileName), configDefaultFile.String())

	return outputs
}

func (c ConfigGenerator) Finally() {
}

func NewConfigGenerator(envVars []envconfig.EnvVar, fileName string) *ConfigGenerator {
	return &ConfigGenerator{
		envVars:  envVars,
		fileName: fileName,
	}
}
