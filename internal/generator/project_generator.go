package generator

import (
	"fmt"
	"gitee.com/eden-framework/eden-framework/internal/generator/files"
	"gitee.com/eden-framework/eden-framework/internal/project"
	"gitee.com/eden-framework/eden-framework/pkg/constant/enum"
	"github.com/AlecAivazis/survey/v2/core"
	"github.com/sirupsen/logrus"
	"path"
	"strings"
)

type expressBool bool

func (e *expressBool) WriteAnswer(field string, value interface{}) error {
	result := value.(core.OptionAnswer)
	switch result.Value {
	case "是":
		*e = true
	case "否":
		*e = false
	}
	return nil
}

type ProjectOption struct {
	Name            string
	Group           string
	Owner           string
	Namespace       string
	Desc            string
	Version         string
	ProgramLanguage string `survey:"project_language"`
	Workflow        string
	ConfigCenter    enum.ConfigCenterType `survey:"config_center"`
}

func NewProjectGenerator(opt ProjectOption) *ProjectGenerator {
	p := project.Project{}
	p = p.WithName(opt.Name).
		WithGroup(opt.Group).
		WithOwner(opt.Owner).
		WithNamespace(opt.Namespace).
		WithDesc(opt.Desc).
		WithLanguage(opt.ProgramLanguage)

	if opt.Version != "" {
		p = p.WithVersion(opt.Version)
	}

	if opt.Workflow != "" && opt.Workflow != "custom" {
		p = p.WithWorkflow(opt.Workflow)
	}

	var withApolloFlag string
	if opt.ConfigCenter == enum.CONFIG_CENTER_TYPE__APOLLO {
		withApolloFlag = fmt.Sprintf(
			" -ldflags \"-X gitee.com/eden-framework/apollo.Branch=%s.json\"",
			files.EnvVarInBash(project.EnvKeyCIBranch),
		)
	}
	builder := project.RegisteredBuilders.GetBuilderBy(p.ProgramLanguage)
	if builder == nil {
		logrus.Panicf("unsupported project language: %s", p.ProgramLanguage)
	}
	buildScripts := make([]string, 0, len(builder.BuildScript))
	testScripts := make([]string, 0, len(builder.TestScript))
	for _, s := range builder.BuildScript {
		if strings.Contains(s, "%s") {
			if withApolloFlag != "" {
				buildScripts = append(buildScripts, fmt.Sprintf(s, withApolloFlag))
			} else {
				buildScripts = append(buildScripts, fmt.Sprintf(s, ""))
			}
		} else {
			buildScripts = append(buildScripts, s)
		}
	}
	for _, s := range builder.TestScript {
		testScripts = append(testScripts, s)
	}
	p.Scripts = map[string]project.Script{
		"build": buildScripts,
		"test":  testScripts,
	}

	return &ProjectGenerator{
		project: p,
	}
}

type ProjectGenerator struct {
	project project.Project
}

func (p *ProjectGenerator) Load(path string) {
	p.project.SetEnviron()
}

func (p *ProjectGenerator) Pick() {
}

func (p *ProjectGenerator) Output(outputPath string) Outputs {
	outputs := Outputs{}

	outputs.Add(path.Join(outputPath, "project.yml"), p.project.String())
	outputs.Add(
		path.Join(outputPath, ".drone.yml"),
		p.project.Workflow.TryExtendsOrSetDefaults().ToDroneConfig(&p.project).String(),
	)
	outputs.Add(
		path.Join(outputPath, ".gitlab-ci.yml"),
		p.project.Workflow.TryExtendsOrSetDefaults().ToCIConfig(&p.project).String(),
	)

	return outputs
}

func (p *ProjectGenerator) Finally() {

}
