package files

import (
	"gitee.com/eden-framework/envconfig"
	"github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"
	"os"
)

type ConfigDefaultFile struct {
	EnvVars []envconfig.EnvVar
}

func NewConfigDefaultFile(envVars []envconfig.EnvVar) *ConfigDefaultFile {
	return &ConfigDefaultFile{EnvVars: envVars}
}

func (f *ConfigDefaultFile) String() string {
	e := make(map[string]string)

	e["GOENV"] = os.Getenv("GOENV")

	for _, envVar := range f.EnvVars {
		e[envVar.Key] = envVar.GetValue(false)
	}

	bytes, err := yaml.Marshal(e)
	if err != nil {
		logrus.Panic(err)
	}
	return string(bytes)
}
