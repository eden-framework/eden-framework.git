package generator

import (
	"fmt"
	"gitee.com/eden-framework/eden-framework/pkg/constant/enum"
	"os"
	"os/exec"
	"path"

	"gitee.com/eden-framework/eden-framework/internal/generator/files"
	"github.com/sirupsen/logrus"
)

type ServiceOption struct {
	FrameworkVersion string `survey:"framework_version"`
	Name             string
	PackageName      string                `survey:"package_name"`
	DatabaseSupport  expressBool           `survey:"database_support"`
	ConfigCenter     enum.ConfigCenterType `survey:"config_center"`

	Group           string
	Owner           string
	Namespace       string
	Desc            string
	Version         string
	ProgramLanguage string `survey:"project_language"`
	Workflow        string `survey:"workflow"`
}

type ServiceGenerator struct {
	opt ServiceOption
}

func NewServiceGenerator(opt ServiceOption) *ServiceGenerator {
	s := &ServiceGenerator{
		opt: opt,
	}

	return s
}

func (s *ServiceGenerator) Load(path string) {
}

func (s *ServiceGenerator) Pick() {
}

func (s *ServiceGenerator) mustModInit() {
	cmd := exec.Command("go", "mod", "init", s.opt.PackageName)
	logrus.Infof("command executing: %s", cmd.String())
	err := cmd.Run()
	if err != nil {
		logrus.Panicf("go mod init err: %v\n", err)
	}
}

func (s *ServiceGenerator) mustGetFramework() {
	cmd := exec.Command("go", "get", fmt.Sprintf("gitee.com/eden-framework/eden-framework@%s", s.opt.FrameworkVersion))
	logrus.Infof("command executing: %s", cmd.String())
	err := cmd.Run()
	if err != nil {
		logrus.Panicf("go get err: %v\n", err)
	}
}

func (s *ServiceGenerator) mustModTidy() {
	cmd := exec.Command("go", "mod", "tidy")
	logrus.Infof("command executing: %s", cmd.String())
	err := cmd.Run()
	if err != nil {
		logrus.Panicf("go mod tidy err: %v\n", err)
	}
}

func (s *ServiceGenerator) Output(outputPath string) Outputs {
	outputs := Outputs{}

	// create service directory
	p := path.Join(outputPath, s.opt.Name)
	createPath(p)

	err := os.Chdir(p)
	if err != nil {
		logrus.Panicf("os.Chdir failed: %v\n", err)
	}

	// go.mod file init
	s.mustModInit()

	// go get framework
	s.mustGetFramework()

	// config center file
	if s.opt.ConfigCenter == enum.CONFIG_CENTER_TYPE__APOLLO {
		apolloFile := s.createApolloFile(p)
		outputs.WriteFile(apolloFile.FileFullName, apolloFile.String())
	} else if s.opt.ConfigCenter == enum.CONFIG_CENTER_TYPE__NACOS {
		nacosFile := s.createNacosFile(p)
		outputs.WriteFile(nacosFile.FileFullName, nacosFile.String())
	} else if s.opt.ConfigCenter == enum.CONFIG_CENTER_TYPE__ETCD {
		etcdFile := s.createEtcdFile(p)
		outputs.WriteFile(etcdFile.FileFullName, etcdFile.String())
	}

	// db config file
	if s.opt.DatabaseSupport {
		dbFile := s.createDbConfigFile(p)
		outputs.WriteFile(dbFile.FileFullName, dbFile.String())
	}

	// general config file
	configFile := s.createConfigFile(p)
	outputs.WriteFile(configFile.FileFullName, configFile.String())

	// status error file
	statusErrorFile := s.createStatusErrorFile(p)
	outputs.WriteFile(statusErrorFile.FileFullName, statusErrorFile.String())

	// router v0 root files
	routerV0RootFile := s.createRouterV0RootFile(p)
	outputs.WriteFile(routerV0RootFile.FileFullName, routerV0RootFile.String())

	// router root files
	routerRootFile := s.createRouterRootFile(p)
	outputs.WriteFile(routerRootFile.FileFullName, routerRootFile.String())

	// main file
	mainFile := s.createMainFile(p)
	outputs.Add(mainFile.FileFullName, mainFile.String())

	return outputs
}

func (s *ServiceGenerator) Finally() {
	s.mustModTidy()
}

func createPath(p string) {
	if !PathExist(p) {
		err := os.Mkdir(p, 0755)
		if err != nil {
			logrus.Panicf("os.Mkdir failed: %v, path: %s", err, p)
		}
		return
	}
	logrus.Panicf("os.Stat exist: %s", p)
}

func (s *ServiceGenerator) createApolloFile(cwd string) *files.GoFile {
	file := files.NewGoFile("global", path.Join(cwd, "internal/global/apollo.go"))
	file.WithBlock(
		fmt.Sprintf(
			`
var ApolloConfig = {{ .UseWithoutAlias "gitee.com/eden-framework/apollo" "" }}.ApolloBaseConfig{
	AppId:            "%s",
	Host:             "localhost:8080",
	BackupConfigPath: "./apollo_config",
	Cluster:          "default",
}
`, s.opt.Name,
		),
	)

	return file
}

func (s *ServiceGenerator) createNacosFile(cwd string) *files.GoFile {
	file := files.NewGoFile("global", path.Join(cwd, "internal/global/nacos.go"))
	file.WithBlock(
		fmt.Sprintf(
			`
var NacosConfig = {{ .UseWithoutAlias "gitee.com/eden-framework/nacos" "" }}.BaseConfig{}
`,
		),
	)

	return file
}

func (s *ServiceGenerator) createEtcdFile(cwd string) *files.GoFile {
	file := files.NewGoFile("global", path.Join(cwd, "internal/global/etcd.go"))
	file.WithBlock(
		fmt.Sprintf(
			`
var EtcdConfig = {{ .UseWithoutAlias "gitee.com/eden-framework/etcd" "" }}.BaseConfig{}
`,
		),
	)

	return file
}

func (s *ServiceGenerator) createDbConfigFile(cwd string) *files.GoFile {
	file := files.NewGoFile("databases", path.Join(cwd, "internal/databases/db.go"))
	file.WithBlock(
		`
var Config = struct {
	DB *{{ .UseWithoutAlias "gitee.com/eden-framework/sqlx" "" }}.Database
}{
	DB: &{{ .UseWithoutAlias "gitee.com/eden-framework/sqlx" "" }}.Database{},
}
`,
	)

	return file
}

func (s *ServiceGenerator) createStatusErrorFile(cwd string) *files.GoFile {
	file := files.NewGoFile("errors", path.Join(cwd, "internal/common/errors/status_error_codes.go"))
	file.WithBlock(
		`
//go:generate eden generate error
const ServiceStatusErrorCode = 100 * 1e3 // todo rename this

const (
	// 请求参数错误
	BadRequest {{ .UseWithoutAlias "gitee.com/eden-framework/courier/status_error" "" }}.StatusErrorCode = http.StatusBadRequest*1e6 + ServiceStatusErrorCode + iota
)

const (
	// 未找到
	NotFound {{ .UseWithoutAlias "gitee.com/eden-framework/courier/status_error" "" }}.StatusErrorCode = http.StatusNotFound*1e6 + ServiceStatusErrorCode + iota
)

const (
	// @errTalk 未授权
	Unauthorized {{ .UseWithoutAlias "gitee.com/eden-framework/courier/status_error" "" }}.StatusErrorCode = http.StatusUnauthorized*1e6 + ServiceStatusErrorCode + iota
)

const (
	// @errTalk 操作冲突
	Conflict {{ .UseWithoutAlias "gitee.com/eden-framework/courier/status_error" "" }}.StatusErrorCode = http.StatusConflict*1e6 + ServiceStatusErrorCode + iota
)

const (
	// @errTalk 不允许操作
	Forbidden {{ .UseWithoutAlias "gitee.com/eden-framework/courier/status_error" "" }}.StatusErrorCode = http.StatusForbidden*1e6 + ServiceStatusErrorCode + iota
)

const (
	// 内部处理错误
	InternalError {{ .UseWithoutAlias "gitee.com/eden-framework/courier/status_error" "" }}.StatusErrorCode = http.StatusInternalServerError*1e6 + ServiceStatusErrorCode + iota
)

const (
	// 上游错误
	BadGateway {{ .UseWithoutAlias "gitee.com/eden-framework/courier/status_error" "" }}.StatusErrorCode = http.StatusBadGateway*1e6 + ServiceStatusErrorCode + iota
)
`,
	)

	return file
}

func (s *ServiceGenerator) createConfigFile(cwd string) *files.GoFile {
	file := files.NewGoFile("global", path.Join(cwd, "internal/global/config.go"))

	file.WithBlock(
		`
var Config = struct {
	LogLevel {{ .UseWithoutAlias "github.com/sirupsen/logrus" "" }}.Level
`,
	)
	if s.opt.DatabaseSupport {
		file.WithBlock(
			`
	// db
	MasterDB *{{ .UseWithoutAlias "gitee.com/eden-framework/eden-framework/pkg/client/mysql" "" }}.MySQL
	SlaveDB  *{{ .UseWithoutAlias "gitee.com/eden-framework/eden-framework/pkg/client/mysql" "" }}.MySQL
`,
		)
	}
	file.WithBlock(
		`
	// administrator
	GRPCServer *{{ .UseWithoutAlias "gitee.com/eden-framework/courier/transport_grpc" "" }}.ServeGRPC
	HTTPServer *{{ .UseWithoutAlias "gitee.com/eden-framework/courier/transport_http" "" }}.ServeHTTP
}{
	LogLevel: {{ .UseWithoutAlias "github.com/sirupsen/logrus" "" }}.DebugLevel,
`,
	)
	if s.opt.DatabaseSupport {
		dbUse := path.Join(s.opt.PackageName, "internal/databases")
		dbPath := path.Join(cwd, "internal/databases")
		file.WithBlock(
			fmt.Sprintf(
				`
	MasterDB: &{{ .UseWithoutAlias "gitee.com/eden-framework/eden-framework/pkg/client/mysql" "" }}.MySQL{Database: {{ .UseWithoutAlias "%s" "%s" }}.Config.DB},
	SlaveDB:  &{{ .UseWithoutAlias "gitee.com/eden-framework/eden-framework/pkg/client/mysql" "" }}.MySQL{Database: {{ .UseWithoutAlias "%s" "%s" }}.Config.DB},
`, dbUse, dbPath, dbUse, dbPath,
			),
		)
	}
	file.WithBlock(
		`
	GRPCServer: &{{ .UseWithoutAlias "gitee.com/eden-framework/courier/transport_grpc" "" }}.ServeGRPC{
		Port: 8900,
	},
	HTTPServer: &{{ .UseWithoutAlias "gitee.com/eden-framework/courier/transport_http" "" }}.ServeHTTP{
		Port:     8800,
		WithCORS: true,
	},
}
`,
	)

	return file
}

func (s *ServiceGenerator) createRouterV0RootFile(cwd string) *files.GoFile {
	file := files.NewGoFile("v0", path.Join(cwd, "internal/routers/v0/root.go"))
	file.WithBlock(
		`
var Router = {{ .UseWithoutAlias "gitee.com/eden-framework/courier" "" }}.NewRouter(V0Router{})

type V0Router struct {
	{{ .UseWithoutAlias "gitee.com/eden-framework/courier" "" }}.EmptyOperator
}

func (V0Router) Path() string {
	return "/v0"
}
`,
	)

	return file
}

func (s *ServiceGenerator) createRouterRootFile(cwd string) *files.GoFile {
	pkgPath := path.Join(s.opt.PackageName, "internal/routers/v0")
	filePath := path.Join(cwd, "internal/routers/v0")

	file := files.NewGoFile("routers", path.Join(cwd, "internal/routers/root.go"))
	file.WithBlock(
		fmt.Sprintf(
			`
var Router = {{ .UseWithoutAlias "gitee.com/eden-framework/courier" "" }}.NewRouter(RootRouter{})

func init() {
	Router.Register({{ .UseWithoutAlias "%s" "%s" }}.Router)
	if !{{ .UseWithoutAlias "gitee.com/eden-framework/context" "" }}.IsOnline() {
		Router.Register({{ .UseWithoutAlias "gitee.com/eden-framework/courier/swagger" "" }}.SwaggerRouter)
	}
}

type RootRouter struct {
	{{ .UseWithoutAlias "gitee.com/eden-framework/courier" "" }}.EmptyOperator
}

func (RootRouter) Path() string {
	return "/%s"
}
`, pkgPath, filePath, GetServiceName(s.opt.Name),
		),
	)

	return file
}

func (s *ServiceGenerator) createMainFile(cwd string) *files.GoFile {
	globalPkgPath := path.Join(s.opt.PackageName, "internal/global")
	globalFilePath := path.Join(cwd, "internal/global")

	file := files.NewGoFile("main", path.Join(cwd, "cmd/main.go"))
	file.WithBlock(
		fmt.Sprintf(
			`
var cmdMigrationDryRun bool

func main() {
	app := application.NewApplication(runner, nil,
		{{ .UseWithoutAlias "gitee.com/eden-framework/eden-framework/pkg/application" "" }}.WithConfig(&{{ .UseWithoutAlias "%s" "%s" }}.Config)`,
			globalPkgPath,
			globalFilePath,
		),
	)

	if s.opt.ConfigCenter == enum.CONFIG_CENTER_TYPE__APOLLO {
		file.WithBlock(
			fmt.Sprintf(
				`,
		{{ .UseWithoutAlias "gitee.com/eden-framework/eden-framework/pkg/application" "" }}.WithConfigCenter({{ .UseWithoutAlias "gitee.com/eden-framework/eden-framework/pkg/constant/enum" "" }}.CONFIG_CENTER_TYPE__APOLLO, &{{ .UseWithoutAlias "%s" "%s" }}.ApolloConfig)`,
				globalPkgPath,
				globalFilePath,
			),
		)
	} else if s.opt.ConfigCenter == enum.CONFIG_CENTER_TYPE__NACOS {
		file.WithBlock(
			fmt.Sprintf(
				`,
		{{ .UseWithoutAlias "gitee.com/eden-framework/eden-framework/pkg/application" "" }}.WithConfigCenter({{ .UseWithoutAlias "gitee.com/eden-framework/eden-framework/pkg/constant/enum" "" }}.CONFIG_CENTER_TYPE__NACOS, &{{ .UseWithoutAlias "%s" "%s" }}.NacosConfig)`,
				globalPkgPath,
				globalFilePath,
			),
		)
	} else if s.opt.ConfigCenter == enum.CONFIG_CENTER_TYPE__ETCD {
		file.WithBlock(
			fmt.Sprintf(
				`,
		{{ .UseWithoutAlias "gitee.com/eden-framework/eden-framework/pkg/application" "" }}.WithConfigCenter({{ .UseWithoutAlias "gitee.com/eden-framework/eden-framework/pkg/constant/enum" "" }}.CONFIG_CENTER_TYPE__ETCD, &{{ .UseWithoutAlias "%s" "%s" }}.EtcdConfig)`,
				globalPkgPath,
				globalFilePath,
			),
		)
	}

	if s.opt.DatabaseSupport {
		pkgPath := path.Join(s.opt.PackageName, "internal/databases")
		filePath := path.Join(cwd, "internal/databases")
		file.WithBlock(
			fmt.Sprintf(
				`,
		{{ .UseWithoutAlias "gitee.com/eden-framework/eden-framework/pkg/application" "" }}.WithConfig(&{{ .UseWithoutAlias "%s" "%s" }}.Config)`,
				pkgPath,
				filePath,
			),
		)
	}

	file.WithBlock(
		`)
`,
	)

	if s.opt.DatabaseSupport {
		file.WithBlock(
			`
	cmdMigrate := &{{ .UseWithoutAlias "github.com/spf13/cobra" "" }}.Command{
		Use: "migrate",
		Run: func(cmd *{{ .UseWithoutAlias "github.com/spf13/cobra" "" }}.Command, args []string) {
			migrate(&{{ .UseWithoutAlias "gitee.com/eden-framework/sqlx/migration" "" }}.MigrationOpts{
				DryRun: cmdMigrationDryRun,
			})
		},
	}
	cmdMigrate.Flags().BoolVarP(&cmdMigrationDryRun, "dry", "d", false, "migrate --dry")
	app.AddCommand(cmdMigrate)
`,
		)
	}

	file.WithBlock(
		`
	app.Start()
}
`,
	)

	routerPkgPath := path.Join(s.opt.PackageName, "internal/routers")
	routerFilePath := path.Join(cwd, "internal/routers")

	file.WithBlock(
		fmt.Sprintf(
			`
func runner(ctx *{{ .UseWithoutAlias "gitee.com/eden-framework/context" "" }}.WaitStopContext, _ *testing.T) error {
	{{ .UseWithoutAlias "github.com/sirupsen/logrus" "" }}.SetLevel({{ .UseWithoutAlias "%s" "%s" }}.Config.LogLevel)
	go {{ .UseWithoutAlias "%s" "%s" }}.Config.GRPCServer.Serve(ctx, {{ .UseWithoutAlias "%s" "%s" }}.Router)
	return {{ .UseWithoutAlias "%s" "%s" }}.Config.HTTPServer.Serve(ctx, {{ .UseWithoutAlias "%s" "%s" }}.Router)
}
`,
			globalPkgPath,
			globalFilePath,
			globalPkgPath,
			globalFilePath,
			routerPkgPath,
			routerFilePath,
			globalPkgPath,
			globalFilePath,
			routerPkgPath,
			routerFilePath,
		),
	)

	if s.opt.DatabaseSupport {
		file.WithBlock(
			fmt.Sprintf(
				`
func migrate(opts *{{ .UseWithoutAlias "gitee.com/eden-framework/sqlx/migration" "" }}.MigrationOpts) {
	if err := {{ .UseWithoutAlias "gitee.com/eden-framework/sqlx/migration" "" }}.Migrate({{ .UseWithoutAlias "%s" "%s" }}.Config.MasterDB, opts); err != nil {
		panic(err)
	}
}
`, globalPkgPath, globalFilePath,
			),
		)
	}

	return file
}
