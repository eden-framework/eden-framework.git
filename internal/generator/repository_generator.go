package generator

import (
	"gitee.com/eden-framework/codegen"
	"gitee.com/eden-framework/eden-framework/internal/generator/scanner"
	"gitee.com/eden-framework/packagex"
	str "gitee.com/eden-framework/strings"
	"github.com/sirupsen/logrus"
	"go/types"
	"os"
	"path"
	"path/filepath"
)

func NewRepositoryGenerator() *RepositoryGenerator {
	return &RepositoryGenerator{}
}

type RepositoryGenerator struct {
	scanner.ModelScannerConfig
	pkg     *packagex.Package
	scanner *scanner.ModelScanner
}

func (g *RepositoryGenerator) Load(cwd string) {
	var err error
	if len(cwd) == 0 {
		cwd, err = os.Getwd()
		if err != nil {
			logrus.Panicf("get current working directory err: %v, cwd: %s", err, cwd)
		}
	}
	_, err = os.Stat(cwd)
	if err != nil {
		if !os.IsExist(err) {
			logrus.Panicf("entry path does not exist: %s", cwd)
		}
	}
	pkg, err := packagex.Load(cwd)
	if err != nil {
		logrus.Panic(err)
	}

	g.pkg = pkg
	g.scanner = scanner.NewModelScanner(pkg, &g.ModelScannerConfig)
}

func (g *RepositoryGenerator) Pick() {
	for ident, obj := range g.pkg.TypesInfo.Defs {
		if typeName, ok := obj.(*types.TypeName); ok {
			if typeName.Name() == g.StructName {
				if _, ok := typeName.Type().Underlying().(*types.Struct); ok {
					g.scanner.Scan(typeName, g.pkg.CommentsOf(ident))
				}
			}
		}
	}
}

func (g *RepositoryGenerator) Output(outputPath string) Outputs {
	if !g.scanner.IsScanned() {
		return nil
	}

	if outputPath == "" {
		outputPath, _ = filepath.Abs("../repository")
	} else {
		if !filepath.IsAbs(outputPath) {
			outputPath, _ = filepath.Abs(outputPath)
		}
	}

	cwd, _ := os.Getwd()
	dir, _ := filepath.Rel(cwd, outputPath)
	controllerFileName := codegen.GeneratedFileSuffix(str.ToLowerSnakeCase(g.StructName) + "/repository.go")
	parameterFileName := codegen.GeneratedFileSuffix(str.ToLowerSnakeCase(g.StructName) + "/parameters.go")

	controllerFile := codegen.NewFile(str.ToLowerSnakeCase(g.StructName), controllerFileName)
	parameterFile := codegen.NewFile(str.ToLowerSnakeCase(g.StructName), parameterFileName)

	g.scanner.WriteRepository(controllerFile, parameterFile)

	return Outputs{
		path.Join(dir, controllerFileName): string(controllerFile.Bytes()),
		path.Join(dir, parameterFileName):  string(parameterFile.Bytes()),
	}
}

func (g *RepositoryGenerator) Finally() {

}
