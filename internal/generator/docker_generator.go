package generator

import (
	"gitee.com/eden-framework/eden-framework/internal"
	"gitee.com/eden-framework/eden-framework/internal/generator/files"
	"gitee.com/eden-framework/envconfig"
	"github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"
	"path"
)

var (
	Image     = files.EnvVar(internal.DockerRegistry) + "/${PROJECT_GROUP}/${PROJECT_NAME}:${PROJECT_VERSION}"
	FromImage = files.EnvVar(internal.DockerRegistry) + "/" + files.EnvVar(internal.RunnerGolang)
)

type DockerGenerator struct {
	ServiceName string
	EnvVars     []envconfig.EnvVar
}

func NewDockerGenerator(serviceName string, envVars []envconfig.EnvVar) *DockerGenerator {
	return &DockerGenerator{
		ServiceName: serviceName,
		EnvVars:     envVars,
	}
}

func (d *DockerGenerator) Load(path string) {
}

func (d *DockerGenerator) Pick() {
}

func (d *DockerGenerator) Output(outputPath string) Outputs {
	outputs := Outputs{}

	dockerFile := &files.Dockerfile{
		From:  FromImage,
		Image: Image,
	}
	dockerFile = dockerFile.AddEnv("GOENV", "DEV")

	for _, envVar := range d.EnvVars {
		strValue := envVar.GetValue(false)
		dockerFile = dockerFile.AddEnv(envVar.Key, strValue)
	}

	dockerFile = dockerFile.WithWorkDir("/go/bin")
	dockerFile = dockerFile.WithCmd("./" + d.ServiceName)

	dockerFile = dockerFile.AddContent("./configs", "./configs")
	dockerFile = dockerFile.AddContent("./build/"+d.ServiceName, "./")
	dockerFile = dockerFile.AddContent("./project.yml", "./")
	dockerFile = dockerFile.AddContent("./api/openapi.json", "./api/openapi.json")

	content, err := yaml.Marshal(dockerFile)
	if err != nil {
		logrus.Panic(err)
	}

	outputs.Add(path.Join(outputPath, "./dockerfile.default.yml"), string(content))
	return outputs
}

func (d *DockerGenerator) Finally() {

}
