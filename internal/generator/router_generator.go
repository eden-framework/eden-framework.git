package generator

import (
	"gitee.com/eden-framework/eden-framework/internal/generator/scanner"
	"gitee.com/eden-framework/packagex"
	"github.com/sirupsen/logrus"
	"go/types"
	"os"
	"path"
	"path/filepath"
)

func NewRouterGenerator() *RouterGenerator {
	return &RouterGenerator{}
}

type RouterGenerator struct {
	scanner.ModelScannerConfig
	pkg               *packagex.Package
	scanner           *scanner.ModelScanner
	controllerPkgPath string
	errorPkgPath      string
}

func (g *RouterGenerator) Load(cwd string) {
	var err error
	if g.ControllerPath == "" {
		logrus.Panic("controller-path must be defined")
	}
	if g.ErrorPath == "" {
		logrus.Panic("error-path must be defined")
	}
	if len(cwd) == 0 {
		cwd, err = os.Getwd()
		if err != nil {
			logrus.Panicf("get current working directory err: %v, cwd: %s", err, cwd)
		}
	}
	_, err = os.Stat(cwd)
	if err != nil {
		if !os.IsExist(err) {
			logrus.Panicf("entry path does not exist: %s", cwd)
		}
	}
	pkg, err := packagex.Load(cwd)
	if err != nil {
		logrus.Panic(err)
	}

	g.pkg = pkg
	g.scanner = scanner.NewModelScanner(pkg, &g.ModelScannerConfig)

	if !filepath.IsAbs(g.ControllerPath) {
		g.ControllerPath, err = filepath.Abs(g.ControllerPath)
		if err != nil {
			logrus.Panic(err)
		}
	}
	controllerPkg, err := packagex.Load(g.ControllerPath)
	if err != nil {
		logrus.Panic(err)
	}
	g.controllerPkgPath = controllerPkg.PkgPath

	if !filepath.IsAbs(g.ErrorPath) {
		g.ErrorPath, err = filepath.Abs(g.ErrorPath)
		if err != nil {
			logrus.Panic(err)
		}
	}
	errPkgPath, err := packagex.Load(g.ErrorPath)
	if err != nil {
		logrus.Panic(err)
	}
	g.errorPkgPath = errPkgPath.PkgPath
}

func (g *RouterGenerator) Pick() {
	for ident, obj := range g.pkg.TypesInfo.Defs {
		if typeName, ok := obj.(*types.TypeName); ok {
			if typeName.Name() == g.StructName {
				if _, ok := typeName.Type().Underlying().(*types.Struct); ok {
					g.scanner.Scan(typeName, g.pkg.CommentsOf(ident))
				}
			}
		}
	}
}

func (g *RouterGenerator) Output(outputPath string) Outputs {
	if !g.scanner.IsScanned() {
		return nil
	}

	if outputPath == "" {
		outputPath, _ = filepath.Abs("../routers/v0")
	} else {
		if !filepath.IsAbs(outputPath) {
			outputPath, _ = filepath.Abs(outputPath)
		}
	}

	var cwd, _ = os.Getwd()
	var dir, _ = filepath.Rel(cwd, outputPath)
	var outputs = Outputs{}

	files := g.scanner.WriteRouters(g.controllerPkgPath, g.errorPkgPath)
	for filePath, file := range files {
		if file != nil {
			outputs.Add(path.Join(dir, filePath), string(file.Bytes()))
		}
	}

	return outputs
}

func (g *RouterGenerator) Finally() {

}
