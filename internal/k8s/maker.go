package k8s

import (
	"bytes"
	"fmt"
	"gitee.com/eden-framework/context"
	"github.com/pkg/errors"
	"io"
	appsv1 "k8s.io/api/apps/v1"
	apiv1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/util/yaml"
	"os"
	"path/filepath"
)

func MakeDeployment(pathToYaml string, prevDeployment *appsv1.Deployment) (
	*appsv1.Deployment,
	error,
) {
	manifest, err := PathToOSFile(pathToYaml)
	if err != nil {
		return nil, err
	}
	manifestData, err := io.ReadAll(manifest)
	if err != nil {
		return nil, err
	}

	envVars := context.EnvVars{}
	envVars.LoadFromEnviron()
	manifestData = []byte(envVars.Parse(string(manifestData)))
	reader := bytes.NewReader(manifestData)

	var deployment *appsv1.Deployment
	if prevDeployment != nil {
		deployment = prevDeployment
	} else {
		deployment = &appsv1.Deployment{}
	}
	if err := yaml.NewYAMLOrJSONDecoder(reader, 100).Decode(deployment); err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("failed to decode file %s into Deployment", pathToYaml))
	}

	return deployment, nil
}

func MakeService(pathToYaml string, prevService *apiv1.Service) (*apiv1.Service, error) {
	manifest, err := PathToOSFile(pathToYaml)
	if err != nil {
		return nil, err
	}
	manifestData, err := io.ReadAll(manifest)
	if err != nil {
		return nil, err
	}

	envVars := context.EnvVars{}
	envVars.LoadFromEnviron()
	manifestData = []byte(envVars.Parse(string(manifestData)))
	reader := bytes.NewReader(manifestData)

	var resource *apiv1.Service
	if prevService != nil {
		resource = prevService
	} else {
		resource = &apiv1.Service{}
	}
	if err := yaml.NewYAMLOrJSONDecoder(reader, 100).Decode(resource); err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("failed to decode file %s into Service", pathToYaml))
	}

	return resource, nil
}

func PathToOSFile(relativePath string) (*os.File, error) {
	path, err := filepath.Abs(relativePath)
	if err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("failed generate absolut file path of %s", relativePath))
	}

	manifest, err := os.Open(path)
	if err != nil {
		return nil, errors.Wrap(err, fmt.Sprintf("failed to open file %s", path))
	}

	return manifest, nil
}
