package internal

const (
	BuilderDeploy  = "BUILDER_DEPLOY"
	BuilderShip    = "BUILDER_SHIP"
	BuilderGolang  = "BUILDER_GOLANG"
	BuilderNextJs  = "BUILDER_NEXTJS"
	BuilderVue     = "BUILDER_VUE"
	DockerRegistry = "DOCKER_REGISTRY"
	RunnerGolang   = "RUNNER_GOLANG"
	RunnerVue      = "RUNNER_VUE"
	RunnerNextJs   = "RUNNER_NEXTJS"
)
