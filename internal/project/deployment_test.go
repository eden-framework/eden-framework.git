package project

import (
	"fmt"
	appsv1 "k8s.io/api/apps/v1"
	v1 "k8s.io/api/core/v1"
	"testing"
)

func Test_deploymentEnvVars(t *testing.T) {
	type args struct {
		deployment *appsv1.Deployment
		envVars    map[string]string
	}
	tests := []struct {
		name      string
		args      args
		wantCount int
	}{
		{
			name: "patch test",
			args: args{
				deployment: &appsv1.Deployment{
					Spec: appsv1.DeploymentSpec{
						Template: v1.PodTemplateSpec{
							Spec: v1.PodSpec{
								Containers: []v1.Container{
									{
										Env: []v1.EnvVar{
											{
												Name:  "hello",
												Value: "world",
											},
										},
									},
								},
							},
						},
					},
				},
				envVars: map[string]string{"hello": "test"},
			},
			wantCount: 1,
		},
		{
			name: "add test",
			args: args{
				deployment: &appsv1.Deployment{
					Spec: appsv1.DeploymentSpec{
						Template: v1.PodTemplateSpec{
							Spec: v1.PodSpec{
								Containers: []v1.Container{
									{
										Env: []v1.EnvVar{
											{
												Name:  "hello",
												Value: "world",
											},
										},
									},
								},
							},
						},
					},
				},
				envVars: map[string]string{"foo": "bar"},
			},
			wantCount: 2,
		},
	}
	for _, tt := range tests {
		t.Run(
			tt.name, func(t *testing.T) {
				patchEnvVars(tt.args.deployment, tt.args.envVars)
				fmt.Printf("%+v", tt.args.deployment.Spec.Template.Spec.Containers[0].Env)
				if len(tt.args.deployment.Spec.Template.Spec.Containers[0].Env) != tt.wantCount {
					t.Errorf(
						"patchEnvVars() = %v, want %v",
						len(tt.args.deployment.Spec.Template.Spec.Containers[0].Env),
						tt.wantCount,
					)
				}
			},
		)
	}
}
