package project

import (
	"fmt"
	"github.com/spf13/viper"
	"os/exec"
)

type DefaultShippingProcessor struct {
}

func (h *DefaultShippingProcessor) Login(p *Project) []*exec.Cmd {
	username := viper.GetString("DOCKER_REGISTRY_USERNAME")
	password := viper.GetString("DOCKER_REGISTRY_PASSWORD")
	registry := viper.GetString("DOCKER_REGISTRY")

	return []*exec.Cmd{
		p.Command(
			"docker", "login",
			fmt.Sprintf("--username=%s", username),
			fmt.Sprintf("--password='%s'", password),
			registry,
		),
	}
}

func (h *DefaultShippingProcessor) Push(p *Project, image string) []*exec.Cmd {
	return []*exec.Cmd{p.Command("docker", "push", image)}
}
