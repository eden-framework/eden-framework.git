package project

import "gitee.com/eden-framework/eden-framework/internal"

const (
	EnvKeyProjectName      = "PROJECT_NAME"
	EnvKeyProjectVersion   = "PROJECT_VERSION"
	EnvKeyProjectOwner     = "PROJECT_OWNER"
	EnvKeyProjectGroup     = "PROJECT_GROUP"
	EnvKeyProjectNamespace = "PROJECT_NAMESPACE"
	EnvKeyProjectFeature   = "PROJECT_FEATURE"
	EnvKeyProjectSelector  = "PROJECT_SELECTOR"

	EnvKeyCICommitSHA   = "DRONE_COMMIT_SHA"
	EnvKeyCIBranch      = "DRONE_BRANCH"
	EnvKeyDeploymentUID = "DEPLOYMENT_UID"

	EnvKeyDockerRegistryKey = internal.DockerRegistry
)
