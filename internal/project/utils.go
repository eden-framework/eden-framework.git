package project

import (
	"os"
	"regexp"
)

func GetBranchName() string {
	return os.Getenv(EnvKeyCIBranch)
}

func GetSha() string {
	ref := os.Getenv(EnvKeyCICommitSHA)
	if len(ref) > 8 {
		return ref[0:8]
	}
	return ref
}

var reFeatureBranch = regexp.MustCompile(`feature/([a-z0-9\-]+)`)

func GetFeatureName() string {
	matched := reFeatureBranch.FindAllStringSubmatch(GetBranchName(), -1)
	if len(matched) > 0 {
		return matched[0][1]
	}
	return ""
}

var reReleaseBranch = regexp.MustCompile(`release/([a-z0-9\-]+)`)

func GetReleaseName() string {
	matched := reReleaseBranch.FindAllStringSubmatch(GetBranchName(), -1)
	if len(matched) > 0 {
		return matched[0][1]
	}
	return ""
}
