package project

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"strings"

	"gitee.com/eden-framework/eden-framework/internal/k8s"
	"gitee.com/eden-framework/eden-framework/pkg/constant/enum"
	str "gitee.com/eden-framework/strings"
	"github.com/fatih/color"
	"github.com/nacos-group/nacos-sdk-go/v2/common/file"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"gopkg.in/yaml.v2"
	appsv1 "k8s.io/api/apps/v1"
	apiv1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"
)

func ProcessDeployment(p *Project, env, deployConfig, serviceConfig string) error {
	if env == "" {
		return errors.New("deployment must specify a environment name")
	}
	var envVars map[string]string
	fmt.Printf("CURRENT env: %s\n", env)
	if strings.ToLower(env) != "prod" {
		envVars = LoadEnv(env, p.Feature)
	}

	kubeConfigKey := str.ToUpperSnakeCase("KubeConfig" + env)
	kubeConfig := viper.GetString(kubeConfigKey)
	if len(kubeConfig) == 0 {
		panic("cannot find kube config file path from .eden.yaml, the key is " + kubeConfigKey)
	}

	ctx, _ := context.WithCancel(context.Background())
	config, err := clientcmd.BuildConfigFromFlags("", kubeConfig)
	if err != nil {
		return err
	}
	clientSet, err := kubernetes.NewForConfig(config)
	if err != nil {
		return err
	}

	// Get Deployment
	deployment, err := k8s.MakeDeployment(deployConfig, nil)
	if err != nil {
		return err
	}

	// check if service use config center
	if file.IsExistFile("./configs/config_center.default.yml") {
		configCenterConfig := viper.GetStringMapString("CONFIG_CENTER_OPTIONS")
		for key, val := range configCenterConfig {
			delete(configCenterConfig, key)
			configCenterConfig[strings.ToUpper(key)] = val
		}
		configCenterType, _ := enum.ParseConfigCenterTypeFromString(viper.GetString("CONFIG_CENTER_TYPE"))
		if configCenterType == enum.CONFIG_CENTER_TYPE__NACOS {
			configCenterConfig[viper.GetString("CONFIG_CENTER_DATAID_KEY")] = p.Name
			configCenterConfig[viper.GetString("CONFIG_CENTER_GROUP_KEY")] = p.Group
			configCenterConfig[viper.GetString("CONFIG_CENTER_NAMESPACE_KEY")] = getNacosNamespace()
		} else if configCenterType == enum.CONFIG_CENTER_TYPE__ETCD {
			configCenterConfig[viper.GetString("CONFIG_CENTER_DATAID_KEY")] = fmt.Sprintf("/%s/%s", p.Group, p.Name)
		}
		patchEnvVars(deployment, configCenterConfig)
	}
	patchEnvVars(deployment, envVars)

	deploymentsClient := clientSet.AppsV1().Deployments(deployment.Namespace)
	_, err = deploymentsClient.Get(ctx, deployment.Name, metav1.GetOptions{})
	if err != nil {
		// Create Deployment
		fmt.Println("Creating deployment...")
		marshal, _ := yaml.Marshal(deployment)
		fmt.Println(string(marshal))
		deployment, err = deploymentsClient.Create(ctx, deployment, metav1.CreateOptions{})
		if err != nil {
			return err
		}
		fmt.Printf("Created deployment %s.\n", deployment.GetObjectMeta().GetName())
	} else {
		// Patch Deployment
		fmt.Println("Updating deployment...")
		data, err := json.Marshal(deployment)
		if err != nil {
			return err
		}
		fmt.Println(string(data))
		deployment, err = deploymentsClient.Patch(
			ctx,
			deployment.Name,
			types.StrategicMergePatchType,
			data,
			metav1.PatchOptions{},
		)
		if err != nil {
			return err
		}
		fmt.Printf("Updated deployment %s.\n", deployment.GetObjectMeta().GetName())
	}

	SetEnv(EnvKeyDeploymentUID, string(deployment.UID))

	// Get Service
	service, err := k8s.MakeService(serviceConfig, nil)
	if err != nil {
		return err
	}
	servicesClient := clientSet.CoreV1().Services(service.Namespace)
	_, err = servicesClient.Get(ctx, service.Name, metav1.GetOptions{})
	if err != nil {
		// Create Service
		fmt.Println("Creating service...")
		marshal, _ := yaml.Marshal(service)
		fmt.Println(string(marshal))
		service, err = servicesClient.Create(ctx, service, metav1.CreateOptions{})
		if err != nil {
			return err
		}
		fmt.Printf("Created service %s.\n", service.GetObjectMeta().GetName())
	} else {
		// Patch Service
		fmt.Println("Updating service...")
		data, err := json.Marshal(service)
		if err != nil {
			return err
		}
		fmt.Println(string(data))
		service, err = servicesClient.Patch(
			ctx,
			service.Name,
			types.StrategicMergePatchType,
			data,
			metav1.PatchOptions{},
		)
		if err != nil {
			return err
		}
		fmt.Printf("Updated service %s.\n", service.GetObjectMeta().GetName())
	}
	return nil
}

func getNacosNamespace() string {
	branchName := GetBranchName()
	fmt.Printf("branchName: %s\n", branchName)
	if branchName == "dev" || branchName == "develop" {
		return "dev"
	}
	if branchName == "test" {
		return branchName
	}
	if branchName == "master" || branchName == "main" {
		return "pro"
	}
	featureName := GetFeatureName()
	if featureName != "" {
		fmt.Printf("featureName: %s\n", featureName)
		featureName = strings.Replace(strings.ToLower(featureName), "-", "", -1)
		return fmt.Sprintf("dev%s", featureName)
	}
	releaseName := GetReleaseName()
	if releaseName != "" {
		fmt.Printf("releaseName: %s\n", releaseName)
		releaseName = strings.Replace(strings.ToLower(releaseName), "-", "", -1)
		return fmt.Sprintf("test%s", releaseName)
	}
	return ""
}

func patchEnvVars(deployment *appsv1.Deployment, envVars map[string]string) {
	originalEnvVars := make(map[string]*apiv1.EnvVar)
	for i := range deployment.Spec.Template.Spec.Containers[0].Env {
		originalEnvVars[deployment.Spec.Template.Spec.Containers[0].Env[i].Name] = &(deployment.Spec.Template.Spec.Containers[0].Env[i])
	}
	for key, val := range envVars {
		if envVar, ok := originalEnvVars[key]; !ok {
			deployment.Spec.Template.Spec.Containers[0].Env = append(
				deployment.Spec.Template.Spec.Containers[0].Env, apiv1.EnvVar{
					Name:  key,
					Value: val,
				},
			)
		} else {
			envVar.Value = val
		}
	}
}

func LoadEnv(envName string, feature string) map[string]string {
	defaultEnv := loadEnvFromFiles("default", feature)
	if envName != "" {
		extendEnv := loadEnvFromFiles(envName, feature)
		defaultEnv = mergeEnvVars(defaultEnv, extendEnv)
	}

	return defaultEnv
}

func loadEnvFromFiles(envName string, feature string) map[string]string {
	defaultEnv := loadEnvFromFile(envName)
	if feature != "" {
		extendEnv := loadEnvFromFile(envName + "-" + feature)
		defaultEnv = mergeEnvVars(defaultEnv, extendEnv)
	}

	return defaultEnv
}

func loadEnvFromFile(envName string) map[string]string {
	filename := "configs/" + strings.ToLower(envName) + ".yml"
	logrus.Infof("try to load env vars from %s ...", color.GreenString(filename))
	envFileContent, err := os.ReadFile(filename)
	if err != nil {
		return nil
	}

	var envVars map[string]string
	err = yaml.Unmarshal(envFileContent, &envVars)
	if err != nil {
		panic(err)
	}
	for key, value := range envVars {
		SetEnv(key, value)
	}
	return envVars
}

func mergeEnvVars(self map[string]string, source map[string]string) map[string]string {
	for key, val := range source {
		self[key] = val
	}
	return self
}
