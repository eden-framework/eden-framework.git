package project

import (
	"fmt"
	"gitee.com/eden-framework/eden-framework/internal/constant/enum"
	"os/exec"
)

type ShippingProcessor interface {
	Login(p *Project) []*exec.Cmd
	Push(p *Project, image string) []*exec.Cmd
}

func NewShippingProcessor(typ enum.ShippingProcessorType) ShippingProcessor {
	switch typ {
	case enum.SHIPPING_PROCESSOR_TYPE__DEFAULT_REGISTRY:
		return new(DefaultShippingProcessor)
	case enum.SHIPPING_PROCESSOR_TYPE__ALIYUN_REGISTRY:
		return new(AliyunShippingProcessor)
	case enum.SHIPPING_PROCESSOR_TYPE__TENCENT_REGISTRY:
		panic("not implemented")
	default:
		panic(fmt.Sprintf("unsupported shipping processor type: %s", typ))
	}
}
