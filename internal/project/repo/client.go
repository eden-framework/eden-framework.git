package repo

import (
	"fmt"
	"gitee.com/eden-framework/courier/client"
	"gitee.com/eden-framework/envconfig"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"net/http"
	"time"
)

type Client struct {
	tagUri        string
	repoListUri   string
	oauthTokenUri string

	accessToken string
	*client.Client
}

func NewClient(mode, host string, port int16, org string) *Client {
	cli := &Client{
		tagUri:        "/api/v5/repos/" + org + "/%s/tags",
		repoListUri:   "/api/v5/orgs/" + org + "/repos",
		oauthTokenUri: "/oauth/token",
		Client: &client.Client{
			Host:    host,
			Mode:    mode,
			Port:    port,
			Timeout: envconfig.Duration(time.Minute),
		},
	}
	var err error
	cli.accessToken, err = cli.getAccessToken()
	if err != nil {
		logrus.Panic(err)
	}
	cli.MarshalDefaults(cli.Client)
	return cli
}

func (c *Client) getAccessToken() (string, error) {
	var result AccessTokenResponse
	param := &AccessTokenRequest{
		GrantType:    "password",
		UserName:     viper.GetString("GITEE_USERNAME"),
		Password:     viper.GetString("GITEE_PASSWORD"),
		ClientID:     viper.GetString("GITEE_CLIENT_ID"),
		ClientSecret: viper.GetString("GITEE_CLIENT_SECRET"),
		Scope:        "projects user_info",
	}
	request := c.Request("", http.MethodPost, c.oauthTokenUri, param)
	err := request.Do().Into(&result)
	if err != nil {
		return "", err
	}
	if result.Error != "" {
		logrus.Errorf("getAccessToken error: %+v", result)
		return "", fmt.Errorf("%s", result.ErrorDescription)
	}
	return result.AccessToken, nil
}

func (c *Client) GetTags(repoName string) (TagsResponse, error) {
	var resp TagsResponse
	url := fmt.Sprintf(c.tagUri+"?access_token=%s", repoName, c.accessToken)
	request := c.Request("", http.MethodGet, url, nil)
	err := request.Do().Into(&resp)
	if err != nil {
		return nil, err
	}
	return resp, nil
}
