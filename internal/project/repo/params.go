package repo

import (
	"path"
	"time"
)

type AccessTokenRequest struct {
	GrantType    string `in:"formData" name:"grant_type"`
	UserName     string `in:"formData" name:"username"`
	Password     string `in:"formData" name:"password"`
	ClientID     string `in:"formData" name:"client_id"`
	ClientSecret string `in:"formData" name:"client_secret"`
	Scope        string `in:"formData" name:"scope"`
}

type AccessTokenResponse struct {
	Error            string `json:"error,omitempty"`
	ErrorDescription string `json:"error_description,omitempty"`
	AccessToken      string `json:"access_token,omitempty"`
	TokenType        string `json:"token_type,omitempty"`
	ExpiresIn        int64  `json:"expires_in,omitempty"`
	RefreshToken     string `json:"refresh_token,omitempty"`
	Scope            string `json:"scope,omitempty"`
	CreatedAt        int64  `json:"created_at,omitempty"`
}

type Commit struct {
	Sha  string    `json:"sha"`
	Date time.Time `json:"url"`
}

type Tag struct {
	Name   string `json:"name"`
	Commit Commit `json:"commit"`
}

type TagsResponse []Tag

type Repository struct {
	ID       uint32 `json:"id"`
	Name     string `json:"name"`
	FullName string `json:"full_name"`
	HtmlUrl  string `json:"html_url"`
}

func (r Repository) GetPackagePath() string {
	return path.Join("gitee.com", r.FullName)
}

type RepoResponse []Repository
