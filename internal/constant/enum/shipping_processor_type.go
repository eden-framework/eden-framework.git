package enum

// api:enum
//
//go:generate eden generate enum --type-name=ShippingProcessorType
type ShippingProcessorType uint8

// 打包处理器类型
const (
	SHIPPING_PROCESSOR_TYPE_UNKNOWN           ShippingProcessorType = iota
	SHIPPING_PROCESSOR_TYPE__DEFAULT_REGISTRY                       // default registry
	SHIPPING_PROCESSOR_TYPE__ALIYUN_REGISTRY                        // aliyun registry
	SHIPPING_PROCESSOR_TYPE__TENCENT_REGISTRY                       // tencent registry
)
