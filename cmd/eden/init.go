/*
Copyright © 2020 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package main

import (
	"fmt"
	"gitee.com/eden-framework/eden-framework/internal/project"
	"os"
	"path"

	"gitee.com/eden-framework/eden-framework/internal/generator"
	"github.com/AlecAivazis/survey/v2"
	"github.com/spf13/cobra"
)

var initQuestions []*survey.Question

func initialQuestions() {
	initQuestions = []*survey.Question{
		{
			Name: "desc",
			Prompt: &survey.Input{
				Message: "项目描述",
				Default: currentProject.Desc,
			},
			Validate: survey.Required,
		},
		{
			Name: "group",
			Prompt: &survey.Input{
				Message: "项目所属应用",
				Default: currentProject.Group,
			},
			Validate: survey.Required,
		},
		{
			Name: "owner",
			Prompt: &survey.Input{
				Message: "项目所属用户组",
				Default: currentProject.Owner,
			},
			Validate: survey.Required,
		},
		{
			Name: "namespace",
			Prompt: &survey.Input{
				Message: "项目所属命名空间",
				Default: currentProject.Namespace,
			},
			Validate: survey.Required,
		},
		{
			Name: "version",
			Prompt: &survey.Input{
				Message: "项目版本号 (x.x.x)",
				Default: currentProject.Version.String(),
			},
			Validate: survey.Required,
		},
		{
			Name: "project_language",
			Prompt: &survey.Select{
				Message: "项目所用编程语言",
				Options: project.RegisteredBuilders.SupportProgramLanguages(),
				Default: currentProject.ProgramLanguage,
			},
			Validate: survey.Required,
		},
		{
			Name: "workflow",
			Prompt: &survey.Select{
				Message: "项目 workflow",
				Options: append(project.PresetWorkflows.List(), "custom"),
				Default: currentProject.Workflow.Extends,
			},
			Validate: survey.Required,
		},
	}
}

// initCmd represents the init command
var initCmd = &cobra.Command{
	Use:   "init",
	Short: "initialize a project",
	Run: func(cmd *cobra.Command, args []string) {
		if len(currentProject.Name) == 0 {
			cwd, _ := os.Getwd()
			currentProject.Name = path.Base(cwd)
		}

		answers := generator.ProjectOption{}

		var qs = append(
			[]*survey.Question{
				{
					Name: "name",
					Prompt: &survey.Input{
						Message: "项目名称",
						Default: currentProject.Name,
					},
					Validate: survey.Required,
				},
			}, initQuestions...,
		)

		err := survey.Ask(qs, &answers)
		if err != nil {
			fmt.Println(err.Error())
			return
		}

		cwd, _ := os.Getwd()

		gen := generator.NewProjectGenerator(answers)
		generator.Generate(gen, cwd, cwd)
	},
}

func init() {
	rootCmd.AddCommand(initCmd)
}
