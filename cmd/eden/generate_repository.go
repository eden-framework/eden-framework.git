/*
Copyright © 2019 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package main

import (
	"fmt"
	"gitee.com/eden-framework/eden-framework/internal/generator"
	"github.com/spf13/cobra"
)

var (
	cmdGenCtrlFlagTableName  string
	cmdGenCtrlFlagIDClient   bool
	cmdGenCtrlFlagPagination bool
	cmdGenCtrlFlagUniqueID   string
	cmdGenCtrlFlagCreatedAt  string
	cmdGenCtrlFlagUpdatedAt  string
	cmdGenCtrlFlagDeletedAt  string
)

// apiCmd represents the api command
var controllerCmd = &cobra.Command{
	Use:   "repository",
	Short: "generate crud repository layer for your model",
	Long:  fmt.Sprintf("%s\ngenerate model doc", CommandHelpHeader),
	Run: func(cmd *cobra.Command, args []string) {
		if cmdGenCtrlFlagIDClient && cmdGenCtrlFlagUniqueID == "" {
			panic("unique-key must be defined when set with-id-client")
		}

		for _, arg := range args {

			g := generator.NewRepositoryGenerator()
			g.StructName = arg
			g.TableName = cmdGenCtrlFlagTableName
			g.WithIDClient = &cmdGenCtrlFlagIDClient
			g.FieldKeyUniqueID = cmdGenCtrlFlagUniqueID
			g.FieldKeyCreatedAt = cmdGenCtrlFlagCreatedAt
			g.FieldKeyUpdatedAt = cmdGenCtrlFlagUpdatedAt
			g.FieldKeyDeletedAt = cmdGenCtrlFlagDeletedAt

			generator.Generate(g, cmdInputPath, cmdOutputPath)
		}
	},
}

func init() {
	controllerCmd.Flags().
		StringVarP(&cmdGenCtrlFlagTableName, "table-name", "t", "", "custom table name")
	controllerCmd.Flags().
		BoolVarP(&cmdGenCtrlFlagIDClient, "with-id-client", "c", true, "with id client support")
	controllerCmd.Flags().
		BoolVarP(&cmdGenCtrlFlagPagination, "with-pagination", "n", true, "with pagination support")
	controllerCmd.Flags().
		StringVarP(&cmdGenCtrlFlagUniqueID, "unique-key", "u", "", "specify a unique id field key")
	controllerCmd.Flags().
		StringVarP(&cmdGenCtrlFlagCreatedAt, "created-key", "r", "CreatedAt", "specify a created at field key")
	controllerCmd.Flags().
		StringVarP(&cmdGenCtrlFlagUpdatedAt, "updated-key", "p", "UpdatedAt", "specify a updated at field key")
	controllerCmd.Flags().
		StringVarP(&cmdGenCtrlFlagDeletedAt, "deleted-key", "e", "DeletedAt", "specify a deleted at field key")

	generateCmd.AddCommand(controllerCmd)
}
