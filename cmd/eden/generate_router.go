/*
Copyright © 2019 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package main

import (
	"fmt"
	"gitee.com/eden-framework/eden-framework/internal/generator"
	"github.com/spf13/cobra"
)

var (
	cmdGenRouterFlagTableName  string
	cmdGenRouterFlagIDClient   bool
	cmdGenRouterFlagPagination bool
	cmdGenRouterFlagUniqueID   string
	cmdGenRouterFlagCreatedAt  string
	cmdGenRouterFlagUpdatedAt  string
	cmdGenRouterFlagDeletedAt  string

	cmdGenRouterControllerPath string
	cmdGenRouterErrorPath      string
)

// apiCmd represents the api command
var routerCmd = &cobra.Command{
	Use:   "router",
	Short: "generate router for your model",
	Long:  fmt.Sprintf("%s\ngenerate model doc", CommandHelpHeader),
	Run: func(cmd *cobra.Command, args []string) {
		if cmdGenRouterFlagIDClient && cmdGenRouterFlagUniqueID == "" {
			panic("unique-key must be defined when set with-id-client")
		}
		if cmdGenRouterControllerPath == "" {
			panic("controller-path must be defined")
		}
		if cmdGenRouterErrorPath == "" {
			panic("error-path must be defined")
		}

		for _, arg := range args {
			g := generator.NewRouterGenerator()
			g.StructName = arg
			g.TableName = cmdGenRouterFlagTableName
			g.WithIDClient = &cmdGenRouterFlagIDClient
			g.FieldKeyUniqueID = cmdGenRouterFlagUniqueID
			g.FieldKeyCreatedAt = cmdGenRouterFlagCreatedAt
			g.FieldKeyUpdatedAt = cmdGenRouterFlagUpdatedAt
			g.FieldKeyDeletedAt = cmdGenRouterFlagDeletedAt
			g.ControllerPath = cmdGenRouterControllerPath
			g.ErrorPath = cmdGenRouterErrorPath
			generator.Generate(g, cmdInputPath, cmdOutputPath)
		}
	},
}

func init() {
	routerCmd.Flags().
		StringVarP(&cmdGenRouterFlagTableName, "table-name", "t", "", "custom table name")
	routerCmd.Flags().
		BoolVarP(&cmdGenRouterFlagIDClient, "with-id-client", "c", true, "with id client support")
	routerCmd.Flags().
		BoolVarP(&cmdGenRouterFlagPagination, "with-pagination", "n", true, "with pagination support")
	routerCmd.Flags().
		StringVarP(&cmdGenRouterFlagUniqueID, "unique-key", "k", "", "specify a unique id field key")
	routerCmd.Flags().
		StringVarP(&cmdGenRouterFlagCreatedAt, "created-key", "r", "CreatedAt", "specify a created at field key")
	routerCmd.Flags().
		StringVarP(&cmdGenRouterFlagUpdatedAt, "updated-key", "u", "UpdatedAt", "specify a updated at field key")
	routerCmd.Flags().
		StringVarP(&cmdGenRouterFlagDeletedAt, "deleted-key", "d", "DeletedAt", "specify a deleted at field key")
	routerCmd.Flags().
		StringVarP(&cmdGenRouterControllerPath, "controller-path", "l", "", "specify the path to controllers package")
	routerCmd.Flags().
		StringVarP(&cmdGenRouterErrorPath, "error-path", "e", "", "specify the path to errors package")

	generateCmd.AddCommand(routerCmd)
}
