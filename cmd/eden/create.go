/*
Copyright © 2020 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package main

import (
	"fmt"
	"gitee.com/eden-framework/eden-framework/pkg/constant/enum"
	"os"
	"path"

	"gitee.com/eden-framework/eden-framework/internal/generator"
	"gitee.com/eden-framework/eden-framework/internal/project/repo"
	"github.com/AlecAivazis/survey/v2"
	"github.com/AlecAivazis/survey/v2/terminal"
	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

var createCmdInitProject bool

// createCmd represents the create and init command
var createCmd = &cobra.Command{
	Use:   "create",
	Short: "create and initialize a project",
	Run: func(cmd *cobra.Command, args []string) {
		// get the tag list of eden-framework
		fmt.Println("fetch the tag list of eden-framework...")
		cli := repo.NewClient("https", "gitee.com", 80, "eden-framework")
		tags, err := cli.GetTags("eden-framework")
		if err != nil {
			logrus.Panicf("cannot get tag list of repo. err=%v", err)
		}
		var tagList = make([]string, 0)
		for _, t := range tags {
			tagList = append(tagList, t.Name)
		}
		if len(tagList) == 0 {
			logrus.Panic("cannot get tag list of repo. tag list is empty")
		}

		var answers generator.ServiceOption
		var qs = []*survey.Question{
			{
				Name: "framework_version",
				Prompt: &survey.Select{
					Message:  "框架版本",
					PageSize: 5,
					Options:  tagList,
					Default:  tagList[0],
				},
			},
			{
				Name: "name",
				Prompt: &survey.Input{
					Message: "项目名称",
					Default: currentProject.Name,
				},
				Validate: func(ans interface{}) error {
					err := survey.Required(ans)
					if err != nil {
						return err
					}

					cwd, _ := os.Getwd()
					p := path.Join(cwd, ans.(string))
					if generator.PathExist(p) {
						return fmt.Errorf("the path %s already exist", p)
					}
					return nil
				},
			},
			{
				Name: "package_name",
				Prompt: &survey.Input{
					Message: "包名",
				},
				Validate: survey.Required,
			},
			{
				Name: "database_support",
				Prompt: &survey.Select{
					Message: "数据库支持",
					Options: []string{"是", "否"},
					Default: "是",
				},
			},
			{
				Name: "config_center",
				Prompt: &survey.Select{
					Message: "配置中心支持",
					Options: enum.ConfigCenterType(0).StringEnums(),
					Default: "NONE",
				},
			},
		}

		if createCmdInitProject {
			qs = append(qs, initQuestions...)
		}

		err = survey.Ask(qs, &answers)
		if err != nil {
			if err == terminal.InterruptErr {
				return
			}
			panic(err)
		}

		cwd, _ := os.Getwd()

		gen := generator.NewServiceGenerator(answers)
		generator.Generate(gen, cwd, cwd)

		if createCmdInitProject {
			projectOpt := generator.ProjectOption{
				Name:            answers.Name,
				Group:           answers.Group,
				Owner:           answers.Owner,
				Namespace:       answers.Namespace,
				Desc:            answers.Desc,
				Version:         answers.Version,
				ProgramLanguage: answers.ProgramLanguage,
				Workflow:        answers.Workflow,
				ConfigCenter:    answers.ConfigCenter,
			}
			cwd = path.Join(cwd, answers.Name)
			projectGen := generator.NewProjectGenerator(projectOpt)
			generator.Generate(projectGen, cwd, cwd)
		}
	},
}

func init() {
	createCmd.Flags().BoolVarP(&createCmdInitProject, "init", "", true, "init after create")
	rootCmd.AddCommand(createCmd)
}
