package main

import (
	"fmt"
	"gitee.com/eden-framework/context"
	"gitee.com/eden-framework/courier/transport_grpc"
	"gitee.com/eden-framework/courier/transport_http"
	"gitee.com/eden-framework/eden-framework/pkg/application"
	"gitee.com/eden-framework/eden-framework/pkg/constant/enum"
	"gitee.com/eden-framework/nacos"
	"github.com/sirupsen/logrus"
	"testing"
)

var Config = struct {
	LogLevel logrus.Level

	// administrator
	GRPCServer *transport_grpc.ServeGRPC
	HTTPServer *transport_http.ServeHTTP
}{}

var ConfigCenterOption = nacos.BaseConfig{}

func main() {
	app := application.NewApplication(
		runner, nil,
		application.WithConfig(&Config),
		application.WithConfigCenter(enum.CONFIG_CENTER_TYPE__NACOS, &ConfigCenterOption),
	)

	app.Start()
}

func runner(ctx *context.WaitStopContext, t *testing.T) error {
	fmt.Println("started")

	select {}
	return nil
}
